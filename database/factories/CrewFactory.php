<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Crew;
use Faker\Generator as Faker;

$factory->define(Crew::class, function (Faker $faker) {

    return [
        'name' => $faker->name,
        // 'slug' => $faker->word,
        'position' => $faker->jobTitle . '+' . $faker->jobTitle,
        'bio' => $faker->text,
        'birthday' => $faker->date,
        'country' => $faker->country,
        'image' => $faker->imageUrl,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
