<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'type' => $faker->word,
        // 'slug' => $faker->word,
        'description' => $faker->text,
        'poster' => $faker->imageUrl,
        'image' => $faker->imageUrl,
        'rate' => $faker->randomDigitNotNull,
        'year' => $faker->year,
        'duration' => $faker->time,
        'category_id' => $faker->randomDigitNotNull,
        'trailer' => $faker->word,
        'views' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
