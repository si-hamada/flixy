<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\MovieFile;
use Faker\Generator as Faker;

$factory->define(MovieFile::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'movie_id' => $faker->word,
        'path' => $faker->word,
        'percent' => $faker->randomDigitNotNull,
        'quality' => $faker->word,
        'notes' => $faker->text,
        'ep' => $faker->word,
        'se' => $faker->word,
        'duration' => $faker->word,
        'views' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
