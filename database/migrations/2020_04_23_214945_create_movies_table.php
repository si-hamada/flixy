<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoviesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->string('type');
            $table->string('slug')->nullable();
            $table->text('description');
            $table->string('poster');
            $table->string('image');
            $table->double('rate')->default(0);
            $table->integer('year')->default(0000);
            $table->string('duration')->default(00-00-00);
            $table->bigInteger('category_id')->unsigned();
            $table->string('trailer');
            $table->integer('views')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movies');
    }
}
