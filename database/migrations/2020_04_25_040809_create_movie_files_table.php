<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMovieFilesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_files', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
            $table->bigInteger('movie_id')->unsigned();
            $table->string('path');
            $table->integer('percent');
            $table->string('quality');
            $table->text('notes')->nullable();
            $table->double('ep')->default(0);
            $table->double('se')->default(0);
            $table->string('duration')->default(00-00-00);
            $table->integer('views')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('movie_id')->references('id')->on('movies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movie_files');
    }
}
