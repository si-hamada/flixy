<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'super_admin' => [
            'acl' => 'c,r,u,d',
            'users' => 'c,r,u,d',
            'tags' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
            'crews' => 'c,r,u,d',
            'movies' => 'c,r,u,d',
            'movie_files' => 'c,r,u,d',
            'reviews' => 'c,r,u,d',
            'pages' => 'c,r,u,d',
            'subscribers' => 'c,r,u,d',
            'settings' => 'r,u',
            'profile' => 'r,u'
        ],
        'uploader' => [
            'tags' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
            'crews' => 'c,r,u,d',
            'movies' => 'c,r,u,d',
            'movie_files' => 'c,r,u,d',
            'profile' => 'r,u'
        ],
        'user' => [
            'profile' => 'r,u',
        ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
