<?php

namespace App\Traits;

trait SecureDelete
{
    /**
     * Delete only when there is no reference to other models.
     *
     * @return response
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($modelDelete) {
            $relationMethods = $modelDelete->relations;

            foreach ($relationMethods as $relationMethod) {
                if ($modelDelete->$relationMethod()->count() > 0) {
                    return false;
                }
            }
        });
    }
}
