<?php

namespace App\Traits;

trait hasGallery
{
    /**
     * Delete only when there is no reference to other models.
     *
     * @return response
     */
    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {

            $gallery = $model->gallery->pluck('file_name')->toArray();

            if (count($gallery) > 0) {
                foreach ($model->gallery as $media) {
                    if (!in_array($media->file_name, request()->input('gallery', []))) {
                        $media->delete();
                    }
                }
            }

            foreach (request()->input('gallery', []) as $file) {
                if (count($gallery) === 0 || !in_array($file, $gallery)) {
                    $model->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('gallery');
                }
            }

        });

    }
}
