<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use FFMpeg\Format\Video\X264;
use Pbmedia\LaravelFFMpeg\FFMpegFacade;

class UploadMovieFileJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $movieFile;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($movieFile)
    {
        $this->movieFile = $movieFile->getFirstMedia('path');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // dd($this->movieFile);
        $lowBitrate = (new X264('aac'))->setKiloBitrate(50);
        $midBitrate = (new X264('aac'))->setKiloBitrate(100);
        $highBitrate = (new X264('aac'))->setKiloBitrate(150);

        FFMpegFacade::fromDisk('public')
            ->open("media/{$this->movieFile->id}/{$this->movieFile->file_name}")
            ->exportForHLS()
            ->setSegmentLength(10) // optional
            ->addFormat($lowBitrate)
            ->addFormat($midBitrate)
            ->addFormat($highBitrate)
            ->save("enc/MovieFiles/{$this->movieFile->model_id}/{$this->movieFile->name}.m3u8");
    }
}
