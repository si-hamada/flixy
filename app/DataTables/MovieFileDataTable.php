<?php

namespace App\DataTables;

use App\Models\MovieFile;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class MovieFileDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', 'backend.movie_files.datatables_actions')
            ->editColumn('created_at', function ($query) {
                return ($query->created_at->format('d/m/Y'));
            })
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\MovieFile $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MovieFile $model)
    {
        return $model->newQuery()->with('movie');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => false,
                // 'order'     => [[0, 'desc']],
                'buttons'   => [
                    [
                        'extend' => 'create',
                        'className' => 'btn btn-primary btn-sm no-corner',
                        'text' => '<i class="fa fa-plus"></i> ' . __('auth.app.create') . ''
                    ],
                    [
                        'extend' => 'export',
                        'className' => 'btn btn-primary btn-sm no-corner',
                        'text' => '<i class="fa fa-download"></i> ' . __('auth.app.export') . ''
                    ],
                    [
                        'extend' => 'print',
                        'className' => 'btn btn-primary btn-sm no-corner',
                        'text' => '<i class="fa fa-print"></i> ' . __('auth.app.print') . ''
                    ],
                    [
                        'extend' => 'reset',
                        'className' => 'btn btn-primary btn-sm no-corner',
                        'text' => '<i class="fa fa-undo"></i> ' . __('auth.app.reset') . ''
                    ],
                    [
                        'extend' => 'reload',
                        'className' => 'btn btn-primary btn-sm no-corner',
                        'text' => '<i class="fa fa-refresh"></i> ' . __('auth.app.reload') . ''
                    ],
                ],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.12/i18n/English.json'),
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            '#' => new Column(['title' => '#', 'data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false]),
            'name' => new Column(['title' => __('models/movie_files.fields.name'), 'data' => 'name']),
            'movie' => new Column(['title' => __('models/movie_files.fields.movie'), 'data' => 'movie.name']),
            'path' => new Column(['title' => __('models/movie_files.fields.path'), 'data' => 'path']),
            'quality' => new Column(['title' => __('models/movie_files.fields.quality'), 'data' => 'quality']),
            'ep' => new Column(['title' => __('models/movie_files.fields.ep'), 'data' => 'ep', 'searchable' => false]),
            'se' => new Column(['title' => __('models/movie_files.fields.se'), 'data' => 'se', 'searchable' => false]),
            // 'duration' => new Column(['title' => __('models/movie_files.fields.duration'), 'data' => 'duration', 'searchable' => false]),
            'created_at' => new Column(['title' => __('models/movie_files.fields.created_at'), 'data' => 'created_at', 'searchable' => false])
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'movieFiles_datatable_' . time();
    }
}
