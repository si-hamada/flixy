<?php

namespace App\Repositories;

use App\Models\MovieFile;
use App\Repositories\BaseRepository;

/**
 * Class MovieFileRepository
 * @package App\Repositories
 * @version April 25, 2020, 4:08 am UTC
*/

class MovieFileRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'movie_id',
        'path',
        'quality'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MovieFile::class;
    }
}
