<?php

namespace App\Repositories;

use App\Models\Crew;
use App\Repositories\BaseRepository;

/**
 * Class CrewRepository
 * @package App\Repositories
 * @version April 22, 2020, 11:58 am UTC
*/

class CrewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'position'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Crew::class;
    }
}
