<?php

namespace App\Models;

use App\Traits\SecureDelete;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Category
 * @package App\Models
 * @version April 22, 2020, 11:08 am UTC
 *
 * @property string name
 * @property string content
 */
class Category extends Model
{
    use SoftDeletes, HasSlug, SecureDelete;

    public $table = 'categories';

    protected $dates = ['deleted_at'];

    protected $relations = ['movies'];

    public $fillable = [
        'name',
        'content'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
        'content' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:tags|regex:/^[a-zA-Z0-9\s]+$/',
        'slug' => 'nullable',
        'content' => 'nullable'
    ];

    public function movies()
    {
        return $this->hasMany(Movie::class);
    }
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
