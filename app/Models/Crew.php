<?php

namespace App\Models;

use App\Traits\hasGallery;
use Carbon\Carbon;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Crew
 * @package App\Models
 * @version April 22, 2020, 11:40 am UTC
 *
 * @property string name
 * @property string position
 * @property string bio
 * @property string birthday
 * @property string country
 * @property string image
 */
class Crew extends Model implements HasMedia
{
    use SoftDeletes, HasSlug, HasMediaTrait, hasGallery;

    public $table = 'crews';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'position',
        'bio',
        'birthday',
        'country',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
        'position' => 'string',
        'bio' => 'string',
        // 'birthday' => 'date',
        'country' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|regex:/^[a-zA-Z0-9\s]+$/|unique:crews,name',
        'slug' => 'nullable',
        'position' => 'required',
        'bio' => 'required',
        'birthday' => 'required',
        'country' => 'required',
        'image' => 'required|image',
        'tags' => 'nullable|array',
        'tags.*' => 'integer',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /* Relations */
    public function movies()
    {
        return $this->belongsToMany(\App\Models\Movie::class)->withPivot('crewAs');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('image')
            ->singleFile()
            ->useFallbackUrl('/backend-files/assets/images/big/img3.jpg');
    }

    /* Accessors */
    public function getImageAttribute()
    {
        return url($this->getFirstMediaUrl('image'));
    }

    public function getGalleryAttribute()
    {
        return $this->getMedia('gallery')->map(function ($g) {
            $g->getFullUrl = $g->getFullUrl();
            return $g;
        });
    }

    /* Scopes */
    public function scopeWhenSearch($query, $search)
    {
        return $query->when($search, function ($q) use ($search) {
            return $q->where('name', 'like', "%$search%");
        });
    }

    public function scopeWhenTags($query, $tags)
    {
        $tags = array_filter($tags);
        return $query->when($tags, function ($q) use ($tags) {
            return $q->whereHas('tags', function ($qu) use ($tags) {
                return $qu->whereIn('tag_id', (array) $tags)
                    ->orWhereIn('id', (array) $tags);
            });
        });
    }

    public function scopeWhenYear($query, $from, $to)
    {
        return $query->when($from, function ($q) use ($from, $to) {
            return $q->whereBetween('birthday', [Carbon::createFromDate($from, 1, 1), Carbon::createFromDate($to, 1, 1)]);
        });
    }

    public function scopeWhenSort($query, $key, $dir)
    {
        return $query->when($key, function ($q) use ($key, $dir) {
            return $q->orderBy($key, $dir);
        });
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }
}
