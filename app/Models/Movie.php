<?php

namespace App\Models;

use App\Traits\hasGallery;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class Movie
 * @package App\Models
 * @version April 23, 2020, 9:49 pm UTC
 *
 * @property \App\Models\Category category
 * @property \Illuminate\Database\Eloquent\Collection tags
 * @property string name
 * @property string type
 * @property string description
 * @property string poster
 * @property string image
 * @property number rate
 * @property integer year
 * @property string duration
 * @property integer category_id
 * @property string trailer
 * @property integer views
 */
class Movie extends Model implements HasMedia
{
    use SoftDeletes, HasSlug, HasMediaTrait, hasGallery;

    public $table = 'movies';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'type',
        'description',
        'poster',
        'image',
        'rate',
        'year',
        'duration',
        'category_id',
        'trailer',
        'views'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'type' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'poster' => 'string',
        'image' => 'string',
        'rate' => 'double',
        'year' => 'integer',
        'duration' => 'string',
        'category_id' => 'integer',
        'trailer' => 'string',
        'views' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|regex:/^[a-zA-Z0-9\s]+$/|unique:movies,name',
        'type' => 'required|in:Movie,Series',
        'slug' => 'nullable',
        'description' => 'required',
        'poster' => 'required|image',
        'image' => 'required|image',
        'rate' => 'required',
        'year' => 'required',
        'duration' => 'required',
        'category_id' => 'required',
        'trailer' => 'required',
        'views' => 'required',
        'tags' => 'nullable|array',
        'tags.*' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function tags()
    {
        return $this->belongsToMany(\App\Models\Tag::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function movieFiles()
    {
        return $this->belongsToMany(\App\Models\MovieFile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function crews()
    {
        return $this->belongsToMany(\App\Models\Crew::class)->withPivot('crewAs');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class)->withPivot('faved', 'later', 'watched');
    }

    public function user()
    {
        return $this->belongsToMany(\App\Models\User::class)->where('user_id', auth()->id())->withPivot('faved', 'later', 'watched')->first();
    }

    public function reviews()
    {
        return $this->hasMany(\App\Models\Review::class);
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('image')
            ->singleFile()
            ->useFallbackUrl('/front/images/uploads/slider-bg2.jpg');
        $this
            ->addMediaCollection('poster')
            ->singleFile()
            ->useFallbackUrl('/front/images/uploads/mv-it8.jpg');
    }

    /* accessors */
    public function getImageAttribute(){
        return url($this->getFirstMediaUrl('image'));
    }

    public function getPosterAttribute(){
        return url($this->getFirstMediaUrl('poster'));
    }

    public function getGalleryAttribute(){
        return $this->getMedia('gallery')->map(function($g){
            $g->getFullUrl = $g->getFullUrl();
            return $g;
        });
    }

    public function getRelatedAttribute(){
        return $this->where('id', '!=', $this->id)->where('category_id', $this->category_id)->limit(5)->get();
    }

    /* scopes */
    public function scopeWhenSearch($query, $search)
    {
        return $query->when($search, function ($q) use ($search) {
            return $q->where('name', 'like', "%$search%")
                ->orWhere('description', 'like', "%$search%");
        });
    }

    public function scopeWhenType($query, $type)
    {
        return $query->when($type, function ($q) use ($type) {
            return $q->where('type', $type);
        });
    }

    public function scopeWhenCategory($query, $category)
    {
        return $query->when($category, function ($q) use ($category) {
            return $q->where('category_id', $category);
        });
    }

    public function scopeWhenTags($query, $tags)
    {
        $tags = array_filter($tags);
        return $query->when($tags, function ($q) use ($tags) {
            return $q->whereHas('tags', function ($qu) use ($tags) {
                return $qu->whereIn('tag_id', (array) $tags)
                    ->orWhereIn('id', (array) $tags);
            });
        });
    }

    public function scopeWhenYear($query, $year)
    {
        return $query->when($year, function ($q) use ($year) {
            return $q->where('year', $year);
        });
    }

    public function scopeWhenRate($query, $from, $to)
    {
        return $query->when($from, function ($q) use ($from) {
            return $q->where('rate', '>=', $from);
        })->when($to, function ($q) use ($to) {
            return $q->where('rate', '<=', $to);
        });
    }

    public function scopeWhenSort($query, $key, $dir)
    {
        return $query->when($key, function ($q) use ($key, $dir) {
            return $q->orderBy($key, $dir);
        });
    }

    public function scopeWhenFavorite($query, $favorite)
    {
        return $query->when($favorite, function ($q) {
            return $q->whereHas('users', function ($qu) {
                return $qu->where('user_id', auth()->user()->id);
            });
        });
    }

    public function scopeWhenUser($query, $what)
    {
        return $query->when($what, function ($q) use ($what) {
            return $q->where($what, 1);
        });
    }

    public function scopeSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }

}
