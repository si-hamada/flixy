<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Subscriber
 * @package App\Models
 * @version April 26, 2020, 12:19 am UTC
 *
 * @property string name
 * @property string email
 */
class Subscriber extends Model
{
    use SoftDeletes;

    public $table = 'subscribers';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'email'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'nullable',
        'email' => 'required|email|unique:subscribers,email'
    ];

}
