<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * Class MovieFile
 * @package App\Models
 * @version April 25, 2020, 4:08 am UTC
 *
 * @property \App\Models\Movie movie
 * @property string name
 * @property integer movie_id
 * @property string path
 * @property integer percent
 * @property string quality
 * @property string notes
 * @property number ep
 * @property number se
 * @property string duration
 * @property integer views
 */
class MovieFile extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'movie_files';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'movie_id',
        'path',
        'percent',
        'quality',
        'notes',
        'ep',
        'se',
        'duration',
        'views'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'movie_id' => 'integer',
        'path' => 'string',
        'percent' => 'integer',
        'quality' => 'string',
        'notes' => 'string',
        'ep' => 'double',
        'se' => 'double',
        'duration' => 'string',
        'views' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'movie_id' => 'required',
        'path' => 'required|file',
        'percent' => 'nullable',
        'quality' => 'required',
        'notes' => 'nullable',
        'ep' => 'required',
        'se' => 'required',
        'duration' => 'nullable',
        'views' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function movie()
    {
        return $this->belongsTo(\App\Models\Movie::class);
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('path')
            ->singleFile();
    }

    public function getPathAttribute()
    {
        return url($this->getFirstMediaUrl('path'));
    }
}
