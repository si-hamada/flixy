<?php

namespace App\Models;

use App\Traits\SecureDelete;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Tag
 * @package App\Models
 * @version April 21, 2020, 7:56 pm UTC
 *
 * @property string name
 * @property string content
 */
class Tag extends Model
{
    use SoftDeletes, HasSlug, SecureDelete;

    public $table = 'tags';

    protected $dates = ['deleted_at'];

    protected $relations = ['movies', 'crews', 'pages'];

    public $fillable = [
        'name',
        'content'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
        'content' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|unique:tags|regex:/^[a-zA-Z0-9\s]+$/',
        'slug' => 'nullable',
        'content' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function movies()
    {
        return $this->belongsToMany(\App\Models\Movie::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function crews()
    {
        return $this->belongsToMany(\App\Models\Crew::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function pages()
    {
        return $this->belongsToMany(\App\Models\Page::class);
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

}
