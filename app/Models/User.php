<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class User extends Authenticatable implements HasMedia
{
    use Notifiable, LaratrustUserTrait, HasMediaTrait;

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'password' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|regex:/^[a-zA-Z0-9\\s]+$/',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:6|confirmed',
        'role' => 'required|integer',
    ];

    public function setPasswordAttribute($password)
    {
        if (!is_null($password) && strlen($password) == 60 && preg_match('/^\$2y\$/', $password)) {
            $this->attributes['password'] = $password;
        } else {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('image')
            ->singleFile()
            ->useFallbackUrl('/front/images/uploads/user-img.png');
    }

    /* Relations */
    public function reviews()
    {
        return $this->hasMany(\App\Models\Review::class);
    }

    public function movies()
    {
        return $this->belongsToMany(\App\Models\Movie::class)->withPivot('faved', 'later', 'watched');
    }


    /* Accessors */
    public function getImageAttribute()
    {
        return url($this->getFirstMediaUrl('image'));
    }

}
