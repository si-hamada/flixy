<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Class Review
 * @package App\Models
 * @version April 25, 2020, 10:32 pm UTC
 *
 * @property \App\Models\Movie movie
 * @property \App\Models\User user
 * @property integer movie_id
 * @property integer user_id
 * @property string title
 * @property string description
 * @property integer rate
 */
class Review extends Model
{
    use SoftDeletes, HasSlug;

    public $table = 'reviews';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'movie_id',
        'user_id',
        'title',
        'description',
        'rate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'movie_id' => 'integer',
        'user_id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'rate' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'movie_id' => 'required',
        'user_id' => 'required',
        'title' => 'nullable',
        'slug' => 'nullable',
        'description' => 'nullable',
        'rate' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function movie()
    {
        return $this->belongsTo(\App\Models\Movie::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

}
