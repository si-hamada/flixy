<?php

namespace App\Providers;
use App\Models\User;
use App\Models\Movie;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Support\ServiceProvider;
use View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['backend.reviews.fields'], function ($view) {
            $userItems = User::pluck('name','id')->toArray();
            $movieItems = Movie::pluck('name','id')->toArray();
            $view->with(['userItems' => $userItems, 'movieItems' => $movieItems]);
        });
        View::composer(['backend.movie_files.fields'], function ($view) {
            $movieItems = Movie::pluck('name','id')->toArray();
            $view->with('movieItems', $movieItems);
        });
        View::composer(['backend.movies.fields'], function ($view) {
            $categoryItems = Category::pluck('name','id')->toArray();
            $tagItems = Tag::pluck('name','id')->toArray();
            $view->with('tagItems', $tagItems)->with('categoryItems', $categoryItems);
        });
        //
    }
}
