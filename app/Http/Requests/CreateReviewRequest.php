<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Review;
use Illuminate\Validation\Rule;

class CreateReviewRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Review::$rules;
        $request = request();
        $rules['movie_id'] = [
            'required',
            Rule::unique('reviews')->where(function ($query) use ($request) {
                return $query->where('user_id', $request->user_id);
            }),
        ];
        return $rules;
    }
}
