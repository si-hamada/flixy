<?php

namespace App\Http\Requests\API;

use App\Models\MovieFile;
use InfyOm\Generator\Request\APIRequest;

class UpdateMovieFileAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = MovieFile::$rules;
        $rules['path'] = $rules['path'].",".$this->route("movie_file");
        return $rules;
    }
}
