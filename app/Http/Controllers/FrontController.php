<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Crew;
use App\Models\Movie;
use App\Models\Review;
use App\Models\Tag;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application FrontPage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        $celebrities = Crew::limit(5)->get();
        return view('front.home')->with(['movies' => $movies, 'celebrities' => $celebrities]);
    }

    public function movies()
    {
        $movies = Movie::whenSearch(request()->search)
            ->whenType(request()->type)
            ->whenCategory(request()->category)
            ->whentags(array_filter((array) request()->tags))
            ->whenYear(request()->year)
            ->whenRate(request()->rate_from, request()->rate_to)
            ->whenSort(request()->sortKey, request()->sortDir)
            ->paginate()
            ->appends(request()->query());

        // if (request()->ajax()) {return $movies->getCollection();}
        // dd($movies);
        // dd(request()->year);

        $tags = Tag::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');
        return view('front.movies')->with([
            'movies' => $movies,
            'tags' => $tags,
            'categories' => $categories,
        ]);
    }

    public function movies_single($slug)
    {
        $movie = Movie::slug($slug)->first();
        // dd($movie->movieFiles->first()->getFirstMedia('path')->name);
        return view('front.movies_single')->with([
            'movie' => $movie
        ]);
    }

    public function movies_review(Request $request, $id)
    {
        $rules = Review::$rules;
        request()->validate($rules, $request->all());
        auth()->user()->reviews()->updateOrCreate([
            'user_id' => $request->user_id,
            'movie_id' => $request->movie_id
        ], $request->all());
        return back();
    }

    public function movies_faved(Request $request, Movie $movie)
    {
        $movie->users()->sync(auth()->id(), false);
        $movie->users()->updateExistingPivot(auth()->id(), ['faved' => !$movie->user()->pivot->faved]);
        return $movie->user()->pivot->faved;
    }

    public function movies_later(Request $request, Movie $movie)
    {
        $movie->users()->sync(auth()->id(), false);
        $movie->users()->updateExistingPivot(auth()->id(), ['later' => !$movie->user()->pivot->later]);
        return $movie->user()->pivot->later;
    }

    public function movies_watched(Request $request, Movie $movie)
    {
        $movie->users()->sync(auth()->id(), false);
        $movie->users()->updateExistingPivot(auth()->id(), ['watched' => !$movie->user()->pivot->watched]);
        return $movie->user()->pivot->watched;
    }

    public function mainSearch()
    {
        $movies = Movie::whenSearch(request()->search)->whenType(request()->type)->get();
        $crew   = Crew::whenSearch(request()->search)->get();

        return $movies->merge($crew);
    }


    public function celebrities()
    {
        $crews = Crew::whenSearch(request()->search)
            ->whentags(array_filter((array) request()->tags))
            ->whenYear(request()->year_from, request()->year_to)
            ->whenSort(request()->sortKey, request()->sortDir)
            ->paginate(5)
            ->appends(request()->query());

        $tags = Tag::pluck('name', 'id');
        return view('front.crews')->with([
            'crews' => $crews,
            'tags' => $tags,
        ]);
    }

    public function celebrities_single($slug)
    {
        $crew = Crew::slug($slug)->first();
        return view('front.crews_single')->with([
            'crew' => $crew
        ]);
    }

    public function profile()
    {
        if (request()->isMethod('post')) {
            if (request()->has('update')) {
                request()->validate([
                    'name' => 'required|regex:/^[a-zA-Z0-9\\s]+$/',
                    'email' => 'required|email|unique:users,email,' . auth()->id()
                ], request()->all());
                auth()->user()->update(request()->only('name'));
            }
            if (request()->has('pass')) {
                request()->validate([
                    'old' => ['required', 'min:6', function ($attribute, $value, $fail) {
                        if (!\Hash::check($value, auth()->user()->password)) {
                            $fail(__('site.Old Password didnt match'));
                        }
                    }],
                    'password' => 'required|min:6|confirmed'
                ], request()->all());
                if (\Hash::check(request()->old, auth()->user()->password)) {
                    auth()->user()->fill([
                        'password' => \Hash::make(request()->password)
                    ])->save();
                }
            }
            \Flash::success(__('messages.updated'));
        }
        return view('front.profile');
    }


    public function user_movies($what)
    {
        $movies = auth()->user()->movies()
            ->whenUser($what)
            ->whenSort(request()->sortKey, request()->sortDir)
            ->paginate();
        return view('front.user_movies')->with([
            'movies' => $movies
        ]);
    }
}
