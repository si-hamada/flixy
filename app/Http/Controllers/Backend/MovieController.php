<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\MovieDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMovieRequest;
use App\Http\Requests\UpdateMovieRequest;
use App\Repositories\MovieRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CrewMovieRequest;
use App\Http\Requests\FilesMovieRequest;
use App\Models\Crew;
use App\Models\Movie;
use App\Models\MovieFile;
use Response;

class MovieController extends AppBaseController
{
    /** @var  MovieRepository */
    private $movieRepository;

    public function __construct(MovieRepository $movieRepo)
    {
        $this->movieRepository = $movieRepo;
    }

    /**
     * Display a listing of the Movie.
     *
     * @param MovieDataTable $movieDataTable
     * @return Response
     */
    public function index(MovieDataTable $movieDataTable)
    {
        return $movieDataTable->render('backend.movies.index');
    }

    /**
     * Show the form for creating a new Movie.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.movies.create');
    }

    /**
     * Store a newly created Movie in storage.
     *
     * @param CreateMovieRequest $request
     *
     * @return Response
     */
    public function store(CreateMovieRequest $request)
    {
        $input = $request->all();

        $movie = $this->movieRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/movies.singular')]));

        return redirect(route('backend.movies.index'));
    }

    /**
     * Display the specified Movie.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            Flash::error(__('models/movies.singular') . ' ' . __('messages.not_found'));

            return redirect(route('backend.movies.index'));
        }

        return view('backend.movies.show')->with('movie', $movie);
    }

    /**
     * Show the form for editing the specified Movie.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $movie = $this->movieRepository->find($id);
        // dd($movie->gallery);
        if (empty($movie)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movies.singular')]));

            return redirect(route('backend.movies.index'));
        }

        return view('backend.movies.edit')->with('movie', $movie);
    }

    /**
     * Update the specified Movie in storage.
     *
     * @param  int              $id
     * @param UpdateMovieRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMovieRequest $request)
    {
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movies.singular')]));

            return redirect(route('backend.movies.index'));
        }

        $input = $request->all();

        $movie = $this->movieRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/movies.singular')]));

        return redirect(route('backend.movies.index'));
    }

    /**
     * Remove the specified Movie from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $movie = $this->movieRepository->find($id);

        if (empty($movie)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movies.singular')]));

            return redirect(route('backend.movies.index'));
        }

        $this->movieRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/movies.singular')]));

        return redirect(route('backend.movies.index'));
    }

    public function crew(CrewMovieRequest $request, Movie $movie)
    {
        if ($request->isMethod('get')) {
            $crews = Crew::select('id', 'name', 'position')->get();
            return view('backend.movies.crew', compact('crews', 'movie'));
        }

        if ($request->isMethod('post')) {
            $movie->crews()->sync($request->crews);
            Flash::success(__('messages.saved', ['model' => __('models/movies.singular')]));
            return redirect(route('backend.movies.index'));
        }
    }

    public function moviefiles(FilesMovieRequest $request, Movie $movie)
    {
        if ($request->isMethod('get')) {
            $moviefiles = MovieFile::select('id', 'name', 'se', 'ep', 'quality')->get();
            return view('backend.movies.moviefiles', compact('moviefiles', 'movie'));
        }

        if ($request->isMethod('post')) {
            $movie->movieFiles()->sync($request->movieFiles);
            Flash::success(__('messages.saved', ['model' => __('models/movies.singular')]));
            return redirect(route('backend.movies.index'));
        }
    }
}
