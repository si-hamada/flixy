<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\TagDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTagRequest;
use App\Http\Requests\UpdateTagRequest;
use App\Repositories\TagRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TagController extends AppBaseController
{
    /** @var  TagRepository */
    private $tagRepository;

    public function __construct(TagRepository $tagRepo)
    {
        $this->tagRepository = $tagRepo;
    }

    /**
     * Display a listing of the Tag.
     *
     * @param TagDataTable $tagDataTable
     * @return Response
     */
    public function index(TagDataTable $tagDataTable)
    {
        return $tagDataTable->render('backend.tags.index');
    }

    /**
     * Show the form for creating a new Tag.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.tags.create');
    }

    /**
     * Store a newly created Tag in storage.
     *
     * @param CreateTagRequest $request
     *
     * @return Response
     */
    public function store(CreateTagRequest $request)
    {
        $input = $request->all();

        $tag = $this->tagRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/tags.singular')]));

        return redirect(route('backend.tags.index'));
    }

    /**
     * Display the specified Tag.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tag = $this->tagRepository->find($id);

        if (empty($tag)) {
            Flash::error(__('models/tags.singular').' '.__('messages.not_found'));

            return redirect(route('backend.tags.index'));
        }

        return view('backend.tags.show')->with('tag', $tag);
    }

    /**
     * Show the form for editing the specified Tag.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tag = $this->tagRepository->find($id);

        if (empty($tag)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tags.singular')]));

            return redirect(route('backend.tags.index'));
        }

        return view('backend.tags.edit')->with('tag', $tag);
    }

    /**
     * Update the specified Tag in storage.
     *
     * @param  int              $id
     * @param UpdateTagRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTagRequest $request)
    {
        $tag = $this->tagRepository->find($id);

        if (empty($tag)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tags.singular')]));

            return redirect(route('backend.tags.index'));
        }

        $tag = $this->tagRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/tags.singular')]));

        return redirect(route('backend.tags.index'));
    }

    /**
     * Remove the specified Tag from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tag = $this->tagRepository->find($id);

        if (empty($tag)) {
            Flash::error(__('messages.not_found', ['model' => __('models/tags.singular')]));

            return redirect(route('backend.tags.index'));
        }

        if ($this->tagRepository->delete($id)){
            Flash::success(__('messages.deleted', ['model' => __('models/tags.singular')]));
        } else {
            Flash::error(__('messages.deleted_error', ['model' => __('models/tags.singular')]));
        }


        return redirect(route('backend.tags.index'));
    }
}
