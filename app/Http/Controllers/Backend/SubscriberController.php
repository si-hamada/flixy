<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\SubscriberDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSubscriberRequest;
use App\Http\Requests\UpdateSubscriberRequest;
use App\Repositories\SubscriberRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SubscriberController extends AppBaseController
{
    /** @var  SubscriberRepository */
    private $subscriberRepository;

    public function __construct(SubscriberRepository $subscriberRepo)
    {
        $this->subscriberRepository = $subscriberRepo;
    }

    /**
     * Display a listing of the Subscriber.
     *
     * @param SubscriberDataTable $subscriberDataTable
     * @return Response
     */
    public function index(SubscriberDataTable $subscriberDataTable)
    {
        return $subscriberDataTable->render('backend.subscribers.index');
    }

    /**
     * Show the form for creating a new Subscriber.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.subscribers.create');
    }

    /**
     * Store a newly created Subscriber in storage.
     *
     * @param CreateSubscriberRequest $request
     *
     * @return Response
     */
    public function store(CreateSubscriberRequest $request)
    {
        $input = $request->all();

        $subscriber = $this->subscriberRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/subscribers.singular')]));

        return redirect(route('backend.subscribers.index'));
    }

    /**
     * Display the specified Subscriber.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subscriber = $this->subscriberRepository->find($id);

        if (empty($subscriber)) {
            Flash::error(__('models/subscribers.singular').' '.__('messages.not_found'));

            return redirect(route('backend.subscribers.index'));
        }

        return view('backend.subscribers.show')->with('subscriber', $subscriber);
    }

    /**
     * Show the form for editing the specified Subscriber.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subscriber = $this->subscriberRepository->find($id);

        if (empty($subscriber)) {
            Flash::error(__('messages.not_found', ['model' => __('models/subscribers.singular')]));

            return redirect(route('backend.subscribers.index'));
        }

        return view('backend.subscribers.edit')->with('subscriber', $subscriber);
    }

    /**
     * Update the specified Subscriber in storage.
     *
     * @param  int              $id
     * @param UpdateSubscriberRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubscriberRequest $request)
    {
        $subscriber = $this->subscriberRepository->find($id);

        if (empty($subscriber)) {
            Flash::error(__('messages.not_found', ['model' => __('models/subscribers.singular')]));

            return redirect(route('backend.subscribers.index'));
        }

        $subscriber = $this->subscriberRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/subscribers.singular')]));

        return redirect(route('backend.subscribers.index'));
    }

    /**
     * Remove the specified Subscriber from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subscriber = $this->subscriberRepository->find($id);

        if (empty($subscriber)) {
            Flash::error(__('messages.not_found', ['model' => __('models/subscribers.singular')]));

            return redirect(route('backend.subscribers.index'));
        }

        $this->subscriberRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/subscribers.singular')]));

        return redirect(route('backend.subscribers.index'));
    }
}
