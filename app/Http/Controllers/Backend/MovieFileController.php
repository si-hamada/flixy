<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\MovieFileDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMovieFileRequest;
use App\Http\Requests\UpdateMovieFileRequest;
use App\Repositories\MovieFileRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Jobs\UploadMovieFileJob;
use Response;

class MovieFileController extends AppBaseController
{
    /** @var  MovieFileRepository */
    private $movieFileRepository;

    public function __construct(MovieFileRepository $movieFileRepo)
    {
        $this->movieFileRepository = $movieFileRepo;
    }

    /**
     * Display a listing of the MovieFile.
     *
     * @param MovieFileDataTable $movieFileDataTable
     * @return Response
     */
    public function index(MovieFileDataTable $movieFileDataTable)
    {
        return $movieFileDataTable->render('backend.movie_files.index');
    }

    /**
     * Show the form for creating a new MovieFile.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.movie_files.create');
    }

    /**
     * Store a newly created MovieFile in storage.
     *
     * @param CreateMovieFileRequest $request
     *
     * @return Response
     */
    public function store(CreateMovieFileRequest $request)
    {
        $input = $request->all();

        $movieFile = $this->movieFileRepository->create($input);

        $movieFile->path = $movieFile
            ->addMedia($request->path)
            ->toMediaCollection('path')->getUrl();
        $movieFile->save();

        $this->dispatch(new UploadMovieFileJob($movieFile));

        Flash::success(__('messages.saved', ['model' => __('models/movie_files.singular')]));

        if ($request->ajax())
            return __('messages.saved', ['model' => __('models/movie_files.singular')]);

        return redirect(route('backend.movieFiles.index'));
    }

    /**
     * Display the specified MovieFile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $movieFile = $this->movieFileRepository->find($id);

        if (empty($movieFile)) {
            Flash::error(__('models/movie_files.singular').' '.__('messages.not_found'));

            return redirect(route('backend.movieFiles.index'));
        }

        return view('backend.movie_files.show')->with('movieFile', $movieFile);
    }

    /**
     * Show the form for editing the specified MovieFile.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $movieFile = $this->movieFileRepository->find($id);

        if (empty($movieFile)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movie_files.singular')]));

            return redirect(route('backend.movieFiles.index'));
        }

        return view('backend.movie_files.edit')->with('movieFile', $movieFile);
    }

    /**
     * Update the specified MovieFile in storage.
     *
     * @param  int              $id
     * @param UpdateMovieFileRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMovieFileRequest $request)
    {
        $movieFile = $this->movieFileRepository->find($id);

        if (empty($movieFile)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movie_files.singular')]));

            return redirect(route('backend.movieFiles.index'));
        }

        $input = $request->all();

        if ($request->hasFile('path')) {
            $input['path'] = $movieFile
                ->addMedia($request->path)
                ->toMediaCollection('path')->getUrl();
            // \Storage::disk('public')->deleteDirectory("enc/MovieFiles/{$movieFile->id}");
            $this->dispatch(new UploadMovieFileJob($movieFile));
        }

        $movieFile = $this->movieFileRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/movie_files.singular')]));
        if ($request->ajax())
            return __('messages.updated', ['model' => __('models/movie_files.singular')]);

        return redirect(route('backend.movieFiles.index'));
    }

    /**
     * Remove the specified MovieFile from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $movieFile = $this->movieFileRepository->find($id);

        if (empty($movieFile)) {
            Flash::error(__('messages.not_found', ['model' => __('models/movie_files.singular')]));

            return redirect(route('backend.movieFiles.index'));
        }

        // \Storage::disk('public')->deleteDirectory("enc/MovieFiles/{$movieFile->id}");
        $this->movieFileRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/movie_files.singular')]));

        return redirect(route('backend.movieFiles.index'));
    }
}
