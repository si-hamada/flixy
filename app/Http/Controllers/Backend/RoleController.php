<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\RoleDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Role;
use Response;

class RoleController extends AppBaseController
{

    public function __construct()
    {
        $this->middleware('permission:read_acl')->only(['index']);
        $this->middleware('permission:create_acl')->only(['create', 'store']);
        $this->middleware('permission:update_acl')->only(['edit', 'update']);
        $this->middleware('permission:delete_acl')->only(['destroy']);
    }

    /**
     * Display a listing of the Role.
     *
     * @param RoleDataTable $roleDataTable
     * @return Response
     */
    public function index(RoleDataTable $roleDataTable)
    {
        return $roleDataTable->render('backend.roles.index');
    }

    /**
     * Show the form for creating a new Role.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.roles.create');
    }

    /**
     * Store a newly created Role in storage.
     *
     * @param CreateRoleRequest $request
     *
     * @return Response
     */
    public function store(CreateRoleRequest $request)
    {
        $input = $request->all();

        $role = Role::create($input);
        $role->attachPermissions($request->permissions);

        Flash::success(__('messages.saved', ['model' => __('models/roles.singular')]));

        return redirect(route('backend.roles.index'));
    }

    /**
     * Display the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $role = Role::find($id);

        if (empty($role)) {
            Flash::error(__('models/roles.singular').' '.__('messages.not_found'));

            return redirect(route('backend.roles.index'));
        }

        return view('backend.roles.show')->with('role', $role);
    }

    /**
     * Show the form for editing the specified Role.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $role = Role::find($id);

        if (empty($role)) {
            Flash::error(__('messages.not_found', ['model' => __('models/roles.singular')]));

            return redirect(route('backend.roles.index'));
        }

        return view('backend.roles.edit')->with('role', $role);
    }

    /**
     * Update the specified Role in storage.
     *
     * @param  int              $id
     * @param UpdateRoleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRoleRequest $request)
    {
        $role = Role::find($id);

        if (empty($role)) {
            Flash::error(__('messages.not_found', ['model' => __('models/roles.singular')]));

            return redirect(route('backend.roles.index'));
        }

        $role->update($request->all());
        $role->syncPermissions($request->permissions);

        Flash::success(__('messages.updated', ['model' => __('models/roles.singular')]));

        return redirect(route('backend.roles.index'));
    }

    /**
     * Remove the specified Role from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);

        if (empty($role)) {
            Flash::error(__('messages.not_found', ['model' => __('models/roles.singular')]));

            return redirect(route('backend.roles.index'));
        }

        $role->delete();

        Flash::success(__('messages.deleted', ['model' => __('models/roles.singular')]));

        return redirect(route('backend.roles.index'));
    }
}
