<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\ReviewDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateReviewRequest;
use App\Http\Requests\UpdateReviewRequest;
use App\Repositories\ReviewRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ReviewController extends AppBaseController
{
    /** @var  ReviewRepository */
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepo)
    {
        $this->reviewRepository = $reviewRepo;
    }

    /**
     * Display a listing of the Review.
     *
     * @param ReviewDataTable $reviewDataTable
     * @return Response
     */
    public function index(ReviewDataTable $reviewDataTable)
    {
        return $reviewDataTable->render('backend.reviews.index');
    }

    /**
     * Show the form for creating a new Review.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.reviews.create');
    }

    /**
     * Store a newly created Review in storage.
     *
     * @param CreateReviewRequest $request
     *
     * @return Response
     */
    public function store(CreateReviewRequest $request)
    {
        $input = $request->all();

        $review = $this->reviewRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/reviews.singular')]));

        return redirect(route('backend.reviews.index'));
    }

    /**
     * Display the specified Review.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            Flash::error(__('models/reviews.singular').' '.__('messages.not_found'));

            return redirect(route('backend.reviews.index'));
        }

        return view('backend.reviews.show')->with('review', $review);
    }

    /**
     * Show the form for editing the specified Review.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            Flash::error(__('messages.not_found', ['model' => __('models/reviews.singular')]));

            return redirect(route('backend.reviews.index'));
        }

        return view('backend.reviews.edit')->with('review', $review);
    }

    /**
     * Update the specified Review in storage.
     *
     * @param  int              $id
     * @param UpdateReviewRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReviewRequest $request)
    {
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            Flash::error(__('messages.not_found', ['model' => __('models/reviews.singular')]));

            return redirect(route('backend.reviews.index'));
        }

        $review = $this->reviewRepository->update($request->all(), $id);

        Flash::success(__('messages.updated', ['model' => __('models/reviews.singular')]));

        return redirect(route('backend.reviews.index'));
    }

    /**
     * Remove the specified Review from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $review = $this->reviewRepository->find($id);

        if (empty($review)) {
            Flash::error(__('messages.not_found', ['model' => __('models/reviews.singular')]));

            return redirect(route('backend.reviews.index'));
        }

        $this->reviewRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/reviews.singular')]));

        return redirect(route('backend.reviews.index'));
    }
}
