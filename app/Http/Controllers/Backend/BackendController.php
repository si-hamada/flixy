<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;

class BackendController extends AppBaseController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.home');
    }

    public function mediaTmp(Request $request)
    {
        $request->validate([
            'file' => 'required|image'
        ]);

        $path = storage_path('tmp/uploads');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $file = $request->file('file');

        $name = uniqid() . '_' . trim($file->getClientOriginalName());

        $file->move($path, $name);

        return response()->json([
            'name'          => $name,
            'original_name' => $file->getClientOriginalName(),
        ]);
    }

    public function settings(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('backend.settings');
        }

        if ($request->isMethod('post')) {
            settings()->set($request->all());
            settings()->save();
            Flash::success(__('messages.saved', ['model' => __('site.settings')]));
            return redirect(route('backend.settings'));
        }
    }
}
