<?php

namespace App\Http\Controllers\Backend;

use App\DataTables\CrewDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCrewRequest;
use App\Http\Requests\UpdateCrewRequest;
use App\Repositories\CrewRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use App\Models\Tag;
use Response;

class CrewController extends AppBaseController
{
    /** @var  CrewRepository */
    private $crewRepository;

    public function __construct(CrewRepository $crewRepo)
    {
        $this->crewRepository = $crewRepo;
    }

    /**
     * Display a listing of the Crew.
     *
     * @param CrewDataTable $crewDataTable
     * @return Response
     */
    public function index(CrewDataTable $crewDataTable)
    {
        return $crewDataTable->render('backend.crews.index');
    }

    /**
     * Show the form for creating a new Crew.
     *
     * @return Response
     */
    public function create()
    {
        $tags = Tag::all()->pluck('name', 'id');
        return view('backend.crews.create', compact('tags'));
    }

    /**
     * Store a newly created Crew in storage.
     *
     * @param CreateCrewRequest $request
     *
     * @return Response
     */
    public function store(CreateCrewRequest $request)
    {
        $input = $request->all();

        $crew = $this->crewRepository->create($input);

        Flash::success(__('messages.saved', ['model' => __('models/crews.singular')]));

        return redirect(route('backend.crews.index'));
    }

    /**
     * Display the specified Crew.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $crew = $this->crewRepository->find($id);

        if (empty($crew)) {
            Flash::error(__('models/crews.singular') . ' ' . __('messages.not_found'));

            return redirect(route('backend.crews.index'));
        }

        return view('backend.crews.show')->with('crew', $crew);
    }

    /**
     * Show the form for editing the specified Crew.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $crew = $this->crewRepository->find($id);

        if (empty($crew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/crews.singular')]));

            return redirect(route('backend.crews.index'));
        }

        $tags = Tag::all()->pluck('name', 'id');
        return view('backend.crews.edit')->with('crew', $crew)->with('tags', $tags);
    }

    /**
     * Update the specified Crew in storage.
     *
     * @param  int              $id
     * @param UpdateCrewRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCrewRequest $request)
    {
        $crew = $this->crewRepository->find($id);

        if (empty($crew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/crews.singular')]));

            return redirect(route('backend.crews.index'));
        }

        $input = $request->all();

        $crew = $this->crewRepository->update($input, $id);

        Flash::success(__('messages.updated', ['model' => __('models/crews.singular')]));

        return redirect(route('backend.crews.index'));
    }

    /**
     * Remove the specified Crew from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $crew = $this->crewRepository->find($id);

        if (empty($crew)) {
            Flash::error(__('messages.not_found', ['model' => __('models/crews.singular')]));

            return redirect(route('backend.crews.index'));
        }

        $this->crewRepository->delete($id);

        Flash::success(__('messages.deleted', ['model' => __('models/crews.singular')]));

        return redirect(route('backend.crews.index'));
    }
}
