<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMovieFileAPIRequest;
use App\Http\Requests\API\UpdateMovieFileAPIRequest;
use App\Models\MovieFile;
use App\Repositories\MovieFileRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MovieFileController
 * @package App\Http\Controllers\API
 */

class MovieFileAPIController extends AppBaseController
{
    /** @var  MovieFileRepository */
    private $movieFileRepository;

    public function __construct(MovieFileRepository $movieFileRepo)
    {
        $this->movieFileRepository = $movieFileRepo;
    }

    /**
     * Display a listing of the MovieFile.
     * GET|HEAD /movieFiles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $movieFiles = $this->movieFileRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $movieFiles->toArray(),
            __('messages.retrieved', ['model' => __('models/movie_files.plural')])
        );
    }

    /**
     * Store a newly created MovieFile in storage.
     * POST /movieFiles
     *
     * @param CreateMovieFileAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMovieFileAPIRequest $request)
    {
        $input = $request->all();

        $movieFile = $this->movieFileRepository->create($input);

        return $this->sendResponse(
            $movieFile->toArray(),
            __('messages.saved', ['model' => __('models/movie_files.singular')])
        );
    }

    /**
     * Display the specified MovieFile.
     * GET|HEAD /movieFiles/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MovieFile $movieFile */
        $movieFile = $this->movieFileRepository->find($id);

        if (empty($movieFile)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/movie_files.singular')])
            );
        }

        return $this->sendResponse(
            $movieFile->toArray(),
            __('messages.retrieved', ['model' => __('models/movie_files.singular')])
        );
    }

    /**
     * Update the specified MovieFile in storage.
     * PUT/PATCH /movieFiles/{id}
     *
     * @param int $id
     * @param UpdateMovieFileAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMovieFileAPIRequest $request)
    {
        $input = $request->all();

        /** @var MovieFile $movieFile */
        $movieFile = $this->movieFileRepository->find($id);

        if (empty($movieFile)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/movie_files.singular')])
            );
        }

        $movieFile = $this->movieFileRepository->update($input, $id);

        return $this->sendResponse(
            $movieFile->toArray(),
            __('messages.updated', ['model' => __('models/movie_files.singular')])
        );
    }

    /**
     * Remove the specified MovieFile from storage.
     * DELETE /movieFiles/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MovieFile $movieFile */
        $movieFile = $this->movieFileRepository->find($id);

        if (empty($movieFile)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/movie_files.singular')])
            );
        }

        $movieFile->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/movie_files.singular')])
        );
    }
}
