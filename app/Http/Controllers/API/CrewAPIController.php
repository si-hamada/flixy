<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCrewAPIRequest;
use App\Http\Requests\API\UpdateCrewAPIRequest;
use App\Models\Crew;
use App\Repositories\CrewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CrewController
 * @package App\Http\Controllers\API
 */

class CrewAPIController extends AppBaseController
{
    /** @var  CrewRepository */
    private $crewRepository;

    public function __construct(CrewRepository $crewRepo)
    {
        $this->crewRepository = $crewRepo;
    }

    /**
     * Display a listing of the Crew.
     * GET|HEAD /crews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $crews = $this->crewRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(
            $crews->toArray(),
            __('messages.retrieved', ['model' => __('models/crews.plural')])
        );
    }

    /**
     * Store a newly created Crew in storage.
     * POST /crews
     *
     * @param CreateCrewAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCrewAPIRequest $request)
    {
        $input = $request->all();

        $crew = $this->crewRepository->create($input);

        return $this->sendResponse(
            $crew->toArray(),
            __('messages.saved', ['model' => __('models/crews.singular')])
        );
    }

    /**
     * Display the specified Crew.
     * GET|HEAD /crews/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Crew $crew */
        $crew = $this->crewRepository->find($id);

        if (empty($crew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/crews.singular')])
            );
        }

        return $this->sendResponse(
            $crew->toArray(),
            __('messages.retrieved', ['model' => __('models/crews.singular')])
        );
    }

    /**
     * Update the specified Crew in storage.
     * PUT/PATCH /crews/{id}
     *
     * @param int $id
     * @param UpdateCrewAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCrewAPIRequest $request)
    {
        $input = $request->all();

        /** @var Crew $crew */
        $crew = $this->crewRepository->find($id);

        if (empty($crew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/crews.singular')])
            );
        }

        $crew = $this->crewRepository->update($input, $id);

        return $this->sendResponse(
            $crew->toArray(),
            __('messages.updated', ['model' => __('models/crews.singular')])
        );
    }

    /**
     * Remove the specified Crew from storage.
     * DELETE /crews/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Crew $crew */
        $crew = $this->crewRepository->find($id);

        if (empty($crew)) {
            return $this->sendError(
                __('messages.not_found', ['model' => __('models/crews.singular')])
            );
        }

        $crew->delete();

        return $this->sendResponse(
            $id,
            __('messages.deleted', ['model' => __('models/crews.singular')])
        );
    }
}
