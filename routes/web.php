<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', 'FrontController@index');
Route::get('/main-search', 'FrontController@mainSearch')->name('mainSearch');

Route::get('/movies', 'FrontController@movies')->name('movies');
Route::get('/movies/{slug}', 'FrontController@movies_single')->name('movies.single');
Route::post('/movies/review/{id}', 'FrontController@movies_review')->name('movies.review');
Route::get('/movies/faved/{movie}', 'FrontController@movies_faved')->name('movies.faved');
Route::get('/movies/later/{movie}', 'FrontController@movies_later')->name('movies.later');
Route::get('/movies/watched/{movie}', 'FrontController@movies_watched')->name('movies.watched');

Route::get('/celebrities', 'FrontController@celebrities')->name('celebrities');
Route::get('/celebrities/{slug}', 'FrontController@celebrities_single')->name('celebrities.single');

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::match(['get', 'post'], '/profile', 'FrontController@profile')->name('profile');
    Route::get('/profile/movies/{what}', 'FrontController@user_movies')->name('user_movies');
});

// Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider')->where('provider', 'facebook|google');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->where('provider', 'facebook|google');
