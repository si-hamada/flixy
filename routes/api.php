<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('tags', 'TagAPIController');
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('categories', 'CategoryAPIController');
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('crews', 'CrewAPIController');
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('movies', 'MovieAPIController');
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('movie_files', 'MovieFileAPIController');
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('reviews', 'ReviewAPIController');
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('pages', 'PageAPIController');
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('subscribers', 'SubscriberAPIController');
});


Route::group(['prefix' => 'backend'], function () {
    Route::resource('users', 'UserAPIController');
});
