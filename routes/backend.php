<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BackendController@index')->name('home');
Route::post('/mediaTmp', 'BackendController@mediaTmp')->name('mediaTmp');

Route::resource('tags', 'TagController');

Route::resource('categories', 'CategoryController');

Route::resource('crews', 'CrewController');

Route::resource('movies', 'MovieController');
Route::match(['get', 'post'], 'movies/{movie}/crew', 'MovieController@crew')->name('movies.crew');
Route::match(['get', 'post'], 'movies/{movie}/moviefiles', 'MovieController@moviefiles')->name('movies.moviefiles');

Route::resource('movieFiles', 'MovieFileController');

Route::resource('reviews', 'ReviewController');

Route::resource('pages', 'PageController');

Route::resource('subscribers', 'SubscriberController');

Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::match(['get', 'post'], 'settings', 'BackendController@settings')->name('settings');
