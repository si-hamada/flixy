<?php namespace Tests\Repositories;

use App\Models\MovieFile;
use App\Repositories\MovieFileRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MovieFileRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MovieFileRepository
     */
    protected $movieFileRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->movieFileRepo = \App::make(MovieFileRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_movie_file()
    {
        $movieFile = factory(MovieFile::class)->make()->toArray();

        $createdMovieFile = $this->movieFileRepo->create($movieFile);

        $createdMovieFile = $createdMovieFile->toArray();
        $this->assertArrayHasKey('id', $createdMovieFile);
        $this->assertNotNull($createdMovieFile['id'], 'Created MovieFile must have id specified');
        $this->assertNotNull(MovieFile::find($createdMovieFile['id']), 'MovieFile with given id must be in DB');
        $this->assertModelData($movieFile, $createdMovieFile);
    }

    /**
     * @test read
     */
    public function test_read_movie_file()
    {
        $movieFile = factory(MovieFile::class)->create();

        $dbMovieFile = $this->movieFileRepo->find($movieFile->id);

        $dbMovieFile = $dbMovieFile->toArray();
        $this->assertModelData($movieFile->toArray(), $dbMovieFile);
    }

    /**
     * @test update
     */
    public function test_update_movie_file()
    {
        $movieFile = factory(MovieFile::class)->create();
        $fakeMovieFile = factory(MovieFile::class)->make()->toArray();

        $updatedMovieFile = $this->movieFileRepo->update($fakeMovieFile, $movieFile->id);

        $this->assertModelData($fakeMovieFile, $updatedMovieFile->toArray());
        $dbMovieFile = $this->movieFileRepo->find($movieFile->id);
        $this->assertModelData($fakeMovieFile, $dbMovieFile->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_movie_file()
    {
        $movieFile = factory(MovieFile::class)->create();

        $resp = $this->movieFileRepo->delete($movieFile->id);

        $this->assertTrue($resp);
        $this->assertNull(MovieFile::find($movieFile->id), 'MovieFile should not exist in DB');
    }
}
