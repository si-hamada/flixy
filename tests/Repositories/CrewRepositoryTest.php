<?php namespace Tests\Repositories;

use App\Models\Crew;
use App\Repositories\CrewRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CrewRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CrewRepository
     */
    protected $crewRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->crewRepo = \App::make(CrewRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_crew()
    {
        $crew = factory(Crew::class)->make()->toArray();

        $createdCrew = $this->crewRepo->create($crew);

        $createdCrew = $createdCrew->toArray();
        $this->assertArrayHasKey('id', $createdCrew);
        $this->assertNotNull($createdCrew['id'], 'Created Crew must have id specified');
        $this->assertNotNull(Crew::find($createdCrew['id']), 'Crew with given id must be in DB');
        $this->assertModelData($crew, $createdCrew);
    }

    /**
     * @test read
     */
    public function test_read_crew()
    {
        $crew = factory(Crew::class)->create();

        $dbCrew = $this->crewRepo->find($crew->id);

        $dbCrew = $dbCrew->toArray();
        $this->assertModelData($crew->toArray(), $dbCrew);
    }

    /**
     * @test update
     */
    public function test_update_crew()
    {
        $crew = factory(Crew::class)->create();
        $fakeCrew = factory(Crew::class)->make()->toArray();

        $updatedCrew = $this->crewRepo->update($fakeCrew, $crew->id);

        $this->assertModelData($fakeCrew, $updatedCrew->toArray());
        $dbCrew = $this->crewRepo->find($crew->id);
        $this->assertModelData($fakeCrew, $dbCrew->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_crew()
    {
        $crew = factory(Crew::class)->create();

        $resp = $this->crewRepo->delete($crew->id);

        $this->assertTrue($resp);
        $this->assertNull(Crew::find($crew->id), 'Crew should not exist in DB');
    }
}
