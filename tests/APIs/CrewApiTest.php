<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Crew;

class CrewApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_crew()
    {
        $crew = factory(Crew::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/backend/crews', $crew
        );

        $this->assertApiResponse($crew);
    }

    /**
     * @test
     */
    public function test_read_crew()
    {
        $crew = factory(Crew::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/backend/crews/'.$crew->id
        );

        $this->assertApiResponse($crew->toArray());
    }

    /**
     * @test
     */
    public function test_update_crew()
    {
        $crew = factory(Crew::class)->create();
        $editedCrew = factory(Crew::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/backend/crews/'.$crew->id,
            $editedCrew
        );

        $this->assertApiResponse($editedCrew);
    }

    /**
     * @test
     */
    public function test_delete_crew()
    {
        $crew = factory(Crew::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/backend/crews/'.$crew->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/backend/crews/'.$crew->id
        );

        $this->response->assertStatus(404);
    }
}
