<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MovieFile;

class MovieFileApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_movie_file()
    {
        $movieFile = factory(MovieFile::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/backend/movie_files', $movieFile
        );

        $this->assertApiResponse($movieFile);
    }

    /**
     * @test
     */
    public function test_read_movie_file()
    {
        $movieFile = factory(MovieFile::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/backend/movie_files/'.$movieFile->id
        );

        $this->assertApiResponse($movieFile->toArray());
    }

    /**
     * @test
     */
    public function test_update_movie_file()
    {
        $movieFile = factory(MovieFile::class)->create();
        $editedMovieFile = factory(MovieFile::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/backend/movie_files/'.$movieFile->id,
            $editedMovieFile
        );

        $this->assertApiResponse($editedMovieFile);
    }

    /**
     * @test
     */
    public function test_delete_movie_file()
    {
        $movieFile = factory(MovieFile::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/backend/movie_files/'.$movieFile->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/backend/movie_files/'.$movieFile->id
        );

        $this->response->assertStatus(404);
    }
}
