<?php

return [

    'record'      => 'record',
    'add_new'      => 'Add New',
    'cancel'       => 'Cancel',
    'cancelled'    => 'Cancelled!',
    'save'         => 'Save',
    'delete'       => 'Delete',
    'deleted'      => 'Deleted!',
    'remove'       => 'Remove',
    'edit'         => 'Edit',
    'detail'       => 'Detail',
    'back'         => 'Back',
    'action'       => 'Action',
    'id'           => 'Id',
    'created_at'   => 'Created At',
    'updated_at'   => 'Updated At',
    'deleted_at'   => 'Deleted At',
    'are_you_sure' => 'Are you sure?',
    'you_wont_recover' => 'You will not be able to recover this imaginary file!',
    'you_will_delete'  => 'You will delete',
    'yes_delete'  => 'Yes, delete it!',
    'no_delete'  => 'No, cancel plx!',
    'record_deleted'  => 'Your record file has been deleted.',
    'record_safe'  => 'Your record file is safe :)',
];
