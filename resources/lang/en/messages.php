<?php

return [

    'retrieved' => ':model retrieved successfully.',
    'saved'     => ':model saved successfully.',
    'updated'   => ':model updated successfully.',
    'deleted'   => ':model deleted successfully.',
    'deleted_error'   => ':model can\'t be deleted, check relations.',
    'not_found' => ':model not found',

];
