<?php

return array(
    'singular' => 'Crew',
    'plural' => 'Crews',
    'fields' =>
    array(
        'id' => 'Id',
        'name' => 'Name',
        'slug' => 'Slug',
        'position' => 'Position',
        'bio' => 'Bio',
        'birthday' => 'Birthday',
        'country' => 'Country',
        'image' => 'Image',
        'tags' => 'Tags',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ),
);
