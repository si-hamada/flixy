<?php

return array(
    'singular' => 'Movie',
    'plural' => 'Movies',
    'fields' =>
    array(
        'id' => 'Id',
        'name' => 'Name',
        'type' => 'Type',
        'slug' => 'Slug',
        'description' => 'Description',
        'poster' => 'Poster',
        'image' => 'Image',
        'rate' => 'Rate',
        'year' => 'Year',
        'duration' => 'Duration',
        'category_id' => 'Category Id',
        'category' => 'Category',
        'tags' => 'Tags',
        'trailer' => 'Trailer',
        'views' => 'Views',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
    ),
);
