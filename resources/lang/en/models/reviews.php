<?php

return array (
  'singular' => 'Review',
  'plural' => 'Reviews',
  'fields' => 
  array (
    'id' => 'Id',
    'movie_id' => 'Movie Id',
    'user_id' => 'User Id',
    'title' => 'Title',
    'slug' => 'Slug',
    'description' => 'Description',
    'rate' => 'Rate',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
