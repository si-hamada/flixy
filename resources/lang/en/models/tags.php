<?php

return array (
  'singular' => 'Tag',
  'plural' => 'Tags',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'slug' => 'Slug',
    'content' => 'Content',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
