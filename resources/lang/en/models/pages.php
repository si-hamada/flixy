<?php

return array (
  'singular' => 'Page',
  'plural' => 'Pages',
  'fields' => 
  array (
    'id' => 'Id',
    'title' => 'Title',
    'slug' => 'Slug',
    'content' => 'Content',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
