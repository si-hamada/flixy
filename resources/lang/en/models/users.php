<?php

return array (
  'singular' => 'User',
  'plural' => 'Users',
  'fields' =>
  array (
    'id' => 'Id',
    'name' => 'Name',
    'email' => 'Email',
    'email_verified_at' => 'Email Verified At',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'remember_token' => 'Remember Token',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
