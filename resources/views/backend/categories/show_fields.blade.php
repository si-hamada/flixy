<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/categories.fields.id').':') !!}
    <p>{{ $category->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/categories.fields.name').':') !!}
    <p>{{ $category->name }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', __('models/categories.fields.slug').':') !!}
    <p>{{ $category->slug }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', __('models/categories.fields.content').':') !!}
    <p>{{ $category->content }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/categories.fields.created_at').':') !!}
    <p>{{ $category->created_at }}</p>
</div>

