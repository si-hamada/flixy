@extends('backend.layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">@lang('models/pages.singular')</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">@lang('site.home')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('backend.pages.index') }}">@lang('models/pages.singular')</a></li>
                    <li class="breadcrumb-item active">@lang('crud.edit')</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    @include('adminlte-templates::common.errors')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    {!! Form::model($page, ['route' => ['backend.pages.update', $page->id], 'method' => 'patch']) !!}

                        @include('backend.pages.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
{!! JsValidator::formRequest('App\Http\Requests\UpdatePageRequest', 'form')->ignore('.no-validate'); !!}
@endpush
