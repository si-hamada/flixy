<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/tags.fields.id').':') !!}
    <p>{{ $tag->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/tags.fields.name').':') !!}
    <p>{{ $tag->name }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', __('models/tags.fields.slug').':') !!}
    <p>{{ $tag->slug }}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', __('models/tags.fields.content').':') !!}
    <p>{{ $tag->content }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/tags.fields.created_at').':') !!}
    <p>{{ $tag->created_at }}</p>
</div>

