<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/subscribers.fields.id').':') !!}
    <p>{{ $subscriber->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/subscribers.fields.name').':') !!}
    <p>{{ $subscriber->name }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', __('models/subscribers.fields.email').':') !!}
    <p>{{ $subscriber->email }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/subscribers.fields.created_at').':') !!}
    <p>{{ $subscriber->created_at }}</p>
</div>

