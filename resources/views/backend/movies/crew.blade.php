@extends('backend.layouts.app')

@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">@lang('models/movies.singular'): {{ $movie->name }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        href="{{ route('backend.home') }}">@lang('site.home')</a></li>
                <li class="breadcrumb-item"><a
                        href="{{ route('backend.movies.index') }}">@lang('models/movies.singular')</a>
                </li>
                <li class="breadcrumb-item"><a
                        href="{{ route('backend.movies.edit', $movie->id) }}">{{ $movie->name }}</a>
                </li>
                <li class="breadcrumb-item active">@lang('models/crews.singular')</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

@include('adminlte-templates::common.errors')

<div class="card">
    <div class="card-body">
        <h4 class="text-themecolor">@lang('models/crews.singular')</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                {!! Form::open(['route' => ['backend.movies.crew', $movie->id]]) !!}

                @forelse ($movie->crews as $index => $movieCrew)

                <div class="form-group select-container row">
                    <div class="col-sm-4">
                        <select data-last="{{ $movieCrew->id }}" class="selectpicker form-control selectpicker crew" name="crews[{{ $index }}][crew_id]" required>
                            <option value="">@lang('site.select') @lang('models/crews.singular')</option>
                            @forelse($crews as $item)
                                <option {{ $item->id == $movieCrew->id ? 'selected' : '' }} data-positions="{{ $item->position }}" value="{{ $item->id }}">
                                    {{ $item->name }}</option>
                            @empty
                                <option disabled value="">@lang('crud.add_new') @lang('models/crews.singular')
                                    @lang('site.first')</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="text-center col-sm-1">@lang('site.as')</div>
                    <div class="col-sm-4">
                        <select class="selectpicker form-control crewAs" name="crews[{{ $index }}][crewAs]" required>
                            <option value="{{ $movieCrew->pivot->crewAs }}">{{ $movieCrew->pivot->crewAs }}</option>
                        </select>
                    </div>
                    <div class="text-center col-sm-1">
                        <div class="row">
                            <span class="input-group-btn col-sm-6">
                                <button class="btn btn-info add" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                            <span class="input-group-btn col-sm-6">
                                <button class="btn btn-danger remove" type="button"><i class="fa fa-remove"></i></button>
                            </span>
                        </div>
                    </div>
                </div>

                @empty

                <div class="form-group select-container row">
                    <div class="col-sm-4">
                        <select class="selectpicker form-control selectpicker crew" name="crews[0][crew_id]" required>
                            <option value="">@lang('site.select') @lang('models/crews.singular')</option>
                            @forelse($crews as $item)
                                <option data-positions="{{ $item->position }}" value="{{ $item->id }}">
                                    {{ $item->name }}</option>
                            @empty
                                <option disabled value="">@lang('crud.add_new') @lang('models/crews.singular')
                                    @lang('site.first')</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="text-center col-sm-1">@lang('site.as')</div>
                    <div class="col-sm-4">
                        <select class="selectpicker form-control crewAs" name="crews[0][crewAs]" required>
                            <option value="">@lang('site.select') @lang('models/crews.fields.position')</option>
                        </select>
                    </div>
                    <div class="text-center col-sm-1">
                        <div class="row">
                            <span class="input-group-btn col-sm-6">
                                <button class="btn btn-info add" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                            <span class="input-group-btn col-sm-6">
                                <button class="btn btn-danger remove" type="button"><i class="fa fa-remove"></i></button>
                            </span>
                        </div>
                    </div>
                </div>

                @endforelse

                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('backend.movies.index') }}"
                        class="btn btn-default">@lang('crud.cancel')</a>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(function () {

            // Remove form-group
            $('body').on('click', '.btn-danger.remove', function () {
                if($('.select-container').length < 2)
                    return alert('@lang("site.cantRemoveLastOne")')
                    $(this).closest('.form-group').remove()
            })
            // Add form-group
            var counter = {{ $movie->crews->count() }} + 3 // set counter for name array
            $('body').on('click', '.btn-info.add', function () {
                // make clone of the form group
                var clone = $(this).closest('.form-group').clone(true, true)
                $(this).closest('.form-group').after(clone)
                clone.find('select').each(function(){
                    $(this).val('').attr('name', $(this).attr('name').replace($(this).attr('name').split('')[6], counter)).data('last', '0')
                })
                counter++
            })

            // on Crew change set crewAs and no duplicate
            var selected = {{ $movie->crews->pluck('id') }} // hold selected values
            $('body').on('change', '.crew', function () {
                console.log(selected)
                // Check if selected
                if (selected.indexOf(parseInt($(this).val())) > -1){
                    $(this).val($(this).data('last'))
                    return alert('Already selected!');
                }
                // get old value and remove it from selected array
                selected.indexOf($(this).data('last')) !== -1 && selected.splice(selected.indexOf($(this).data('last')), 1)
                // then push new value to the array
                selected.push(parseInt($(this).val()))
                // get crew select item
                var crewAs = $(this).parent().parent().find('.crewAs');
                // create the option and append to
                var options = $(this).find(':selected').data('positions').split('+');
                // make the select empty
                $(crewAs).html('')
                // append the options
                $.each(options, function(key, value) {
                    $(crewAs).append($("<option></option>").attr("value", value).text(value));
                });
                // set the selected as the last
                console.log(selected)
                $(this).data('last', parseInt($(this).val()))

            })
        })

    </script>
@endpush
