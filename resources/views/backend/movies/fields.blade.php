<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/movies.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', __('models/movies.fields.type').':') !!}
    {!! Form::select('type', ['Movie' => 'Movie', 'Series' => 'Series'], null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', __('models/movies.fields.description').':') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', __('models/categories.singular').':') !!}
    {!! Form::select('category_id', $categoryItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tags', __('models/tags.plural').':') !!}
    {!! Form::select('tags[]', $tagItems, null, ['class' => 'form-control select2 no-validate', 'multiple' => 'multiple']) !!}
</div>

<!-- Year Field -->
<div class="form-group col-sm-6">
    {!! Form::label('year', __('models/movies.fields.year').':') !!}
    {!! Form::number('year', null, ['class' => 'form-control']) !!}
</div>

<!-- Trailer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trailer', __('models/movies.fields.trailer').':') !!}
    {!! Form::text('trailer', null, ['class' => 'form-control']) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', __('models/movies.fields.duration').':') !!}
    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Rate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rate', __('models/movies.fields.rate').':') !!}
    {!! Form::number('rate', null, ['class' => 'form-control']) !!}
</div>

<!-- Views Field -->
<div class="form-group col-sm-6">
    {!! Form::label('views', __('models/movies.fields.views').':') !!}
    {!! Form::number('views', null, ['class' => 'form-control']) !!}
</div>

<!-- Poster Field -->
<div class="form-group col-md-6">
    <label for="input-file-now-custom-1">{{ __('models/movies.fields.poster').':' }}</label>
    <input type="file" name="poster" id="input-file-now-custom-1" class="dropify" data-default-file="{{ $movie->poster ?? old('poster') }}" />
</div>
<div class="clearfix"></div>

<!-- Image Field -->
<div class="form-group col-md-6">
    <label for="input-file-now-custom-2">{{ __('models/movies.fields.image').':' }}</label>
    <input type="file" name="image" id="input-file-now-custom-2" class="dropify" data-default-file="{{ $movie->image ?? old('image') }}" />
</div>
<div class="clearfix"></div>

<x-backend.dropZone :items="$movie->gallery ?? old('gallery')"/>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.movies.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

@push('scripts')
<script>
    $(function () {
        $(document).ready(function() {
            // Basic Dropify
            $('.dropify').dropify()
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    })
</script>
@endpush
