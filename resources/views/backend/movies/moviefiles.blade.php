@extends('backend.layouts.app')

@section('content')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">@lang('models/movies.singular'): {{ $movie->name }}</h4>
    </div>
    <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a
                        href="{{ route('backend.home') }}">@lang('site.home')</a></li>
                <li class="breadcrumb-item"><a
                        href="{{ route('backend.movies.index') }}">@lang('models/movies.singular')</a>
                </li>
                <li class="breadcrumb-item"><a
                        href="{{ route('backend.movies.edit', $movie->id) }}">{{ $movie->name }}</a>
                </li>
                <li class="breadcrumb-item active">@lang('models/movie_files.singular')</li>
            </ol>
        </div>
    </div>
</div>
<div class="clearfix"></div>

@include('adminlte-templates::common.errors')

<div class="card">
    <div class="card-body">
        <h4 class="text-themecolor">@lang('models/movie_files.plural')</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                {!! Form::open(['route' => ['backend.movies.moviefiles', $movie->id]]) !!}

                <div class="table-responsive">
                    <table class="table table-hover movieFiles">
                        <thead>
                            <tr>
                                <th>@lang('models/movie_files.fields.name')</th>
                                <th>@lang('models/movie_files.fields.se')</th>
                                <th>@lang('models/movie_files.fields.ep')</th>
                                <th>@lang('models/movie_files.fields.quality')</th>
                                <th>@lang('crud.remove')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($movie->movieFiles as $index => $movieFile)
                                <tr>
                                    {!! Form::hidden('movieFiles[]', $movieFile->id) !!}
                                    <td>{{ $movieFile->name }}</td>
                                    <td>{{ $movieFile->se }}</td>
                                    <td>{{ $movieFile->ep }}</td>
                                    <td>{{ $movieFile->quality }}</td>
                                    <td>
                                        <button data-id="{{ $movieFile->id }}" class="btn btn-danger remove" type="button"><i class="fa fa-remove"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <hr class="my-4">
                <h5>@lang('crud.add_new') @lang('models/movie_files.singular')</h5>
                <div class="clearfix"></div>
                <div class="form-group select-container row">
                    <div class="col-sm-10">
                        <select class="selectpicker form-control selectpicker moviefiles_select" name="moviefiles_arr">
                            <option value="">@lang('site.select') @lang('models/movie_files.singular')</option>
                            @forelse($moviefiles as $item)
                                <option
                                    {{ in_array($item->id, $movie->moviefiles->pluck('id')->toArray()) ? 'disabled' : '' }}
                                    data-name="{{ $item->name }}" data-se="{{ $item->se }}"
                                    data-ep="{{ $item->ep }}" data-quality="{{ $item->quality }}"
                                    value="{{ $item->id }}">
                                    {{ $item->name . ' - ' . $item->se . ' - ' . $item->ep . ' - ' . $item->quality }}
                                </option>
                            @empty
                                <option disabled value="">@lang('crud.add_new') @lang('models/movie_files.singular')
                                    @lang('site.first')</option>
                                    @endforelse
                        </select>
                    </div>
                    <div class="text-center col-sm-2">
                        <div class="row">
                            <span class="input-group-btn col-sm-6">
                                <button class="btn btn-info add" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                </div>

                <!-- Submit Field -->
                <div class="form-group col-sm-12">
                    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
                    <a href="{{ route('backend.movies.index') }}"
                        class="btn btn-default">@lang('crud.cancel')</a>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(function () {

            // Remove form-group
            $('body').on('click', '.btn-danger.remove', function () {
                if ($('.table.movieFiles tr').length < 3)
                    return alert('@lang("site.cantRemoveLastOne")')
                $(this).closest('tr').remove()
                $('.moviefiles_select option[value="' + $(this).data('id') + '"]').attr('disabled', false)
            })
            // Add form-group
            var counter = {{ $movie->crews->count() }} + 3 // set counter for name array
            $('body').on('click', '.btn-info.add', function () {
                // hold the selected option
                var mv_s = $('.moviefiles_select').find(':selected')
                // make sure not empty value
                if (mv_s.val() < 1) return
                // make the new tr HTML
                var clone = `
                    <tr>
                        <input name="movieFiles[]" type="hidden" value="${$(mv_s).val()}">
                        <td>${$(mv_s).data('name')}</td>
                        <td>${$(mv_s).data('se')}</td>
                        <td>${$(mv_s).data('ep')}</td>
                        <td>${$(mv_s).data('quality')}</td>
                        <td>
                            <button data-id="${$(mv_s).val()}" class="btn btn-danger remove" type="button"><i class="fa fa-remove"></i></button>
                        </td>
                    </tr>`
                $('table tbody').append(clone)
                mv_s.attr('disabled', 'disabled')
                mv_s.parent().val('')
            })

            // on Crew change set crewAs and no duplicate
            var selected = {{ $movie->crews->pluck('id') }} // hold selected values
            $('body').on('change', '.crew', function () {
                console.log(selected)
                // Check if selected
                if (selected.indexOf(parseInt($(this).val())) > -1) {
                    $(this).val($(this).data('last'))
                    return alert('Already selected!');
                }
                // get old value and remove it from selected array
                selected.indexOf($(this).data('last')) !== -1 && selected.splice(selected.indexOf($(
                    this).data('last')), 1)
                // then push new value to the array
                selected.push(parseInt($(this).val()))
                // get crew select item
                var crewAs = $(this).parent().parent().find('.crewAs');
                // create the option and append to
                var options = $(this).find(':selected').data('positions').split('+');
                // make the select empty
                $(crewAs).html('')
                // append the options
                $.each(options, function (key, value) {
                    $(crewAs).append($("<option></option>").attr("value", value).text(value));
                });
                // set the selected as the last
                console.log(selected)
                $(this).data('last', parseInt($(this).val()))

            })
        })

    </script>
@endpush
