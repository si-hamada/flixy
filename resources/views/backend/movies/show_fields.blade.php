<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/movies.fields.id').':') !!}
    <p>{{ $movie->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/movies.fields.name').':') !!}
    <p>{{ $movie->name }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', __('models/movies.fields.type').':') !!}
    <p>{{ $movie->type }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', __('models/movies.fields.slug').':') !!}
    <p>{{ $movie->slug }}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', __('models/movies.fields.duration').':') !!}
    <p>{{ $movie->duration }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/movies.fields.description').':') !!}
    <p>{{ $movie->description }}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category', __('models/movies.fields.category').':') !!}
    <p>{{ $movie->category->name }}</p>
</div>

<!-- Tags Field -->
<div class="form-group">
    {!! Form::label('tags', __('models/movies.fields.tags').':') !!}
@empty($movie->tags->toArray())
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.NoTags')</span>
    </p>
@else
    <p>
        @foreach ($movie->tags as $item)
            <span class="btn btn-xs fc btn-primary">{{ $item['name'] }}</span>
        @endforeach
    </p>
@endempty
</div>

<!-- Year Field -->
<div class="form-group">
    {!! Form::label('year', __('models/movies.fields.year').':') !!}
    <p>{{ $movie->year }}</p>
</div>

<!-- Trailer Field -->
<div class="form-group">
    {!! Form::label('trailer', __('models/movies.fields.trailer').':') !!}
    <p>{{ $movie->trailer }}</p>
</div>

<!-- Rate Field -->
<div class="form-group">
    {!! Form::label('rate', __('models/movies.fields.rate').':') !!}
    <p>{{ $movie->rate }}</p>
</div>

<!-- Views Field -->
<div class="form-group">
    {!! Form::label('views', __('models/movies.fields.views').':') !!}
    <p>{{ $movie->views }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/movies.fields.created_at').':') !!}
    <p>{{ $movie->created_at }}</p>
</div>

<!-- Poster Field -->
<div class="form-group">
    {!! Form::label('poster', __('models/movies.fields.poster').':') !!}
    <div>
        <img
        src="{{ $movie->poster }}"
        alt="{{ $movie->poster }}"
        width="500px"
        srcset="{{ $movie->poster }}"
        class="img-thumbnail image-preview"
        >
    </div>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/movies.fields.image').':') !!}
    <div>
        <img
        src="{{ $movie->image }}"
        alt="{{ $movie->image }}"
        width="500px"
        srcset="{{ $movie->image }}"
        class="img-thumbnail image-preview"
        >
    </div>
</div>

<!-- Gallery Field -->
<div class="form-group">
    {!! Form::label('gallery', __('site.gallery').':') !!}
    {{ $movie->gallery->count() }}
@if($movie->gallery->count() < 1)
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.empty')</span>
    </p>
@else
    @foreach ($movie->gallery as $item)
        <div>
            <img
            src="{{ $item->getFullUrl() }}"
            alt="{{ $item->getFullUrl() }}"
            width="500px"
            srcset="{{ $item->getFullUrl() }}"
            class="img-thumbnail image-preview"
            >
        </div>
    @endforeach
@endif
</div>
