{!! Form::open(['route' => ['backend.movies.destroy', $id], 'method' => 'delete', 'class' => 'text-center']) !!}
<div class='btn-group'>
    <a href="{{ route('backend.movies.show', $id) }}" class='btn btn-outline-primary btn-sm'>
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('backend.movies.edit', $id) }}" class='btn btn-outline-info btn-sm'>
        <i class="fa fa-edit"></i>
    </a>
    {!! Form::button('<i class="fa fa-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-sm sa-btn-delete',
        'data-item' => $name,
    ]) !!}
</div>
<div class='btn-group mt-1'>
    <a href="{{ route('backend.movies.crew', $id) }}" class='btn btn-outline-warning btn-sm'>
        <i class="fa fa-users"></i>
    </a>
    <a href="{{ route('backend.movies.moviefiles', $id) }}" class='btn btn-outline-warning btn-sm'>
        <i class="fa fa-file-movie-o"></i>
    </a>
</div>
{!! Form::close() !!}
