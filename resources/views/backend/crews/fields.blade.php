<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/crews.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Position Field -->
<div class="form-group col-sm-6">
    {!! Form::label('position', __('models/crews.fields.position').':') !!}
    {!! Form::text('position', null, ['class' => 'form-control']) !!}
</div>

<!-- Bio Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('bio', __('models/crews.fields.bio').':') !!}
    {!! Form::textarea('bio', null, ['class' => 'form-control']) !!}
</div>

<!-- Birthday Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', __('models/crews.fields.birthday').':') !!}
    {!! Form::date('birthday', null, ['class' => 'form-control','id'=>'birthday']) !!}
</div>

<!-- Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country', __('models/crews.fields.country').':') !!}
    {!! Form::text('country', null, ['class' => 'form-control']) !!}
</div>

<!-- Tags Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tags', __('models/crews.fields.tags').':') !!}
    {!! Form::select('tags[]', $tags, null, ['class' => 'form-control select2 no-validate', 'multiple' => 'multiple']) !!}
</div>
<div class="clearfix"></div>

<!-- Image Field -->
<div class="form-group col-md-6">
    <label for="input-file-now-custom-1">{{ __('models/crews.fields.image').':' }}</label>
    <input type="file" name="image" id="input-file-now-custom-1" class="dropify" data-default-file="{{ $crew->image ?? old('image') }}" />
</div>
<div class="clearfix"></div>

<x-backend.dropZone :items="$crew->gallery ?? old('gallery')"/>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.crews.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

@push('scripts')
<script>
    $(function () {
        $(document).ready(function() {
            // Basic Dropify
            $('.dropify').dropify()
            //Initialize Select2 Elements
            $('.select2').select2()
        });
    })
</script>
@endpush
