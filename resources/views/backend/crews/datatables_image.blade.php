@isset($image)
<img
    src="{{ $image }}"
    alt="{{ $image }}"
    width="150px"
    srcset="{{ $image }}"
    class="img-thumbnail image-preview"
>
@else
<img
    src=""
    alt=""
    width="150px"
    srcset=""
    class="img-thumbnail image-preview"
>
@endisset
