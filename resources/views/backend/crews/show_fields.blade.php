<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/crews.fields.id').':') !!}
    <p>{{ $crew->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/crews.fields.name').':') !!}
    <p>{{ $crew->name }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', __('models/crews.fields.slug').':') !!}
    <p>{{ $crew->slug }}</p>
</div>

<!-- Position Field -->
<div class="form-group">
    {!! Form::label('position', __('models/crews.fields.position').':') !!}
    <p>{{ $crew->position }}</p>
</div>

<!-- Bio Field -->
<div class="form-group">
    {!! Form::label('bio', __('models/crews.fields.bio').':') !!}
    <p>{{ $crew->bio }}</p>
</div>

<!-- Birthday Field -->
<div class="form-group">
    {!! Form::label('birthday', __('models/crews.fields.birthday').':') !!}
    <p>{{ $crew->birthday }}</p>
</div>

<!-- Country Field -->
<div class="form-group">
    {!! Form::label('country', __('models/crews.fields.country').':') !!}
    <p>{{ $crew->country }}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', __('models/crews.fields.image').':') !!}
    <div>
        <img
        src="{{ $crew->image }}"
        alt="{{ $crew->image }}"
        width="500px"
        srcset="{{ $crew->image }}"
        class="img-thumbnail image-preview"
        >
    </div>
</div>

<!-- Tags Field -->
<div class="form-group">
    {!! Form::label('tags', __('models/crews.fields.tags').':') !!}
@empty($crew->tags->toArray())
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.NoTags')</span>
    </p>
@else
    <p>
        @foreach ($crew->tags as $item)
            <span class="btn btn-xs fc btn-primary">{{ $item['name'] }}</span>
        @endforeach
    </p>
@endempty
</div>

<!-- Gallery Field -->
<div class="form-group">
    {!! Form::label('gallery', __('site.gallery').':') !!}
    {{ $crew->gallery->count() }}
@if($crew->gallery->count() < 1)
    <p>
        <span class="btn btn-xs fc btn-danger">@lang('site.empty')</span>
    </p>
@else
    @foreach ($crew->gallery as $item)
        <div>
            <img
            src="{{ $item->getFullUrl() }}"
            alt="{{ $item->getFullUrl() }}"
            width="500px"
            srcset="{{ $item->getFullUrl() }}"
            class="img-thumbnail image-preview"
            >
        </div>
    @endforeach
@endif
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/crews.fields.created_at').':') !!}
    <p>{{ $crew->created_at }}</p>
</div>

