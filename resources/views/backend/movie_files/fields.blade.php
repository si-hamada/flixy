<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/movie_files.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Movie Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('movie_id', 'Movie Id:') !!}
    {!! Form::select('movie_id', $movieItems, null, ['class' => 'form-control']) !!}
</div>

<!-- Path Field -->
<div class="form-group col-md-6">
    <label for="input-file-now-custom-1">{{ __('models/movie_files.fields.path').':' }}</label>
    <input type="file" name="path" id="input-file-now-custom-1" class="dropify" data-default-file="{{ $movieFile->path ?? old('path') }}" />
</div>
<div class="clearfix"></div>

<!-- Percent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('percent', __('models/movie_files.fields.percent').':') !!}
    {!! Form::number('percent', null, ['class' => 'form-control']) !!}
</div>

<!-- Quality Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quality', __('models/movie_files.fields.quality').':') !!}
    {!! Form::text('quality', null, ['class' => 'form-control']) !!}
</div>

<!-- Notes Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('notes', __('models/movie_files.fields.notes').':') !!}
    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
</div>

<!-- Se Field -->
<div class="form-group col-sm-6">
    {!! Form::label('se', __('models/movie_files.fields.se').':') !!}
    {!! Form::number('se', null, ['class' => 'form-control']) !!}
</div>

<!-- Ep Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ep', __('models/movie_files.fields.ep').':') !!}
    {!! Form::number('ep', null, ['class' => 'form-control']) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', __('models/movie_files.fields.duration').':') !!}
    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Views Field -->
<div class="form-group col-sm-6">
    {!! Form::label('views', __('models/movie_files.fields.views').':') !!}
    {!! Form::number('views', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.movieFiles.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>

<div id="uploading-model" class="modal fade">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title">@lang('site.uploading')</h4>
            </div>

            <div class="modal-body">
                <div class="progress m-5">
                    <div class="progress-bar bg-danger active progress-bar-striped" style="width: 0; height:17px;" role="progressbar">
                        0
                    </div>
                </div>
                <div class="error text-center">
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $(function () {
        $(document).ready(function() {
            // Basic Dropify
            $('.dropify').dropify()

            // upload
            $(document).on('submit','form',function(e){
                e.preventDefault()
                // clear the model then trigger it
                $('#uploading-model .modal-footer').toggle()
                $('#uploading-model .modal-body .error').html('')
                $('#uploading-model').modal({
                    backdrop: 'static',
                    keyboard: false
                })
                // collecting data to ajax request
                var url = $(this).attr('action')
                var method = $(this).attr('method')
                var formData = new FormData();
                // loop on form fields and prepare it for request
                $(this).serializeArray().forEach(function(d){
                    formData.append(d.name, d.value);
                })
                // check if there is a movie to upload
                var path = $('form input[name="path"]')
                if (path[0].files[0]){
                    formData.append('path', path[0].files[0])
                }
                // $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('form input[name="_token"]').val()}})
                $.ajax({
                    url: url,
                    data: formData,
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (response) {
                        $('#uploading-model .modal-body .error').html(`<h3>${response}</h3>`)
                        window.location = "{{ route('backend.movieFiles.index') }}"
                    },
                    error: function (error) {
                        // show the response error and show the close button
                        $('#uploading-model .modal-body .error').html(error.responseText)
                        $('#uploading-model .modal-footer').toggle()
                    },
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = Math.round(evt.loaded / evt.total * 100) + "%";
                                $('#uploading-model .progress-bar').css('width', percentComplete).html(percentComplete)
                                console.log(percentComplete)
                            }
                        }, false);
                        return xhr;
                    },
                });//end of ajax call
            });
            // end upload
        });
    })
</script>
@endpush
