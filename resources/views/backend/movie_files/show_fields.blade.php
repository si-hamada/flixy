<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/movie_files.fields.id').':') !!}
    <p>{{ $movieFile->id }}</p>
</div>

<!-- Movie Id Field -->
<div class="form-group">
    {!! Form::label('name', __('models/movie_files.fields.name').':') !!}
    <p>{{ $movieFile->name }}</p>
</div>

<!-- Movie Id Field -->
<div class="form-group">
    {!! Form::label('movie_id', __('models/movie_files.fields.movie_id').':') !!}
    <p>{{ $movieFile->movie_id }}</p>
</div>

<!-- Path Field -->
<div class="form-group">
    {!! Form::label('path', __('models/movie_files.fields.path').':') !!}
    <p>{{ $movieFile->path }}</p>
</div>

<!-- Percent Field -->
<div class="form-group">
    {!! Form::label('percent', __('models/movie_files.fields.percent').':') !!}
    <p>{{ $movieFile->percent }}</p>
</div>

<!-- Quality Field -->
<div class="form-group">
    {!! Form::label('quality', __('models/movie_files.fields.quality').':') !!}
    <p>{{ $movieFile->quality }}</p>
</div>

<!-- Notes Field -->
<div class="form-group">
    {!! Form::label('notes', __('models/movie_files.fields.notes').':') !!}
    <p>{{ $movieFile->notes }}</p>
</div>

<!-- Ep Field -->
<div class="form-group">
    {!! Form::label('ep', __('models/movie_files.fields.ep').':') !!}
    <p>{{ $movieFile->ep }}</p>
</div>

<!-- Se Field -->
<div class="form-group">
    {!! Form::label('se', __('models/movie_files.fields.se').':') !!}
    <p>{{ $movieFile->se }}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', __('models/movie_files.fields.duration').':') !!}
    <p>{{ $movieFile->duration }}</p>
</div>

<!-- Views Field -->
<div class="form-group">
    {!! Form::label('views', __('models/movie_files.fields.views').':') !!}
    <p>{{ $movieFile->views }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/movie_files.fields.created_at').':') !!}
    <p>{{ $movieFile->created_at }}</p>
</div>

