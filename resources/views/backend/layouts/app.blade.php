<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('backend-files') }}/assets/images/favicon.png">
    <title>{{ config('app.name') }}</title>
    <!-- This page CSS -->
    <!-- chartist CSS -->
    <link href="{{ asset('backend-files') }}/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="{{ asset('backend-files') }}/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="{{ asset('backend-files') }}/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{ asset('backend-files') }}/assets/node_modules/dropzone-master/dist/dropzone.css" rel="stylesheet">
    <link href="{{ asset('backend-files') }}/assets/node_modules/dropify/dist/css/dropify.min.css" rel="stylesheet">
    <link href="{{ asset('backend-files') }}/assets/node_modules/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('backend-files') }}/dist/css/style.min.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="{{ asset('backend-files') }}/dist/css/pages/dashboard1.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
    .invalid-feedback{
        z-index: 3;
        position: inherit;
    }
    .dropzone .dz-preview .dz-image img {
        width: 100%;
        height: 100%;
    }
</style>
</head>

<body class="fixed-layout skin-red-dark">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">{{ config('app.name') }} admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        @include('backend.layouts.header')
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @include('backend.layouts.sidebar')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- MAIN CONTENT  -->
                <!-- ============================================================== -->
                @yield('content')
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © {{ now()->year }} Si HaMaDa
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/popper/popper.min.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('backend-files') }}/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="{{ asset('backend-files') }}/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('backend-files') }}/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('backend-files') }}/dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/morrisjs/morris.min.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- Popup message jquery -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/toast-master/js/jquery.toast.js"></script>
    <!-- Chart JS -->
    <script src="{{ asset('backend-files') }}/dist/js/dashboard1.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/dropzone-master/dist/dropzone.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/dropify/dist/js/dropify.min.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/sweetalert/sweetalert.min.js"></script>
    <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/select2/dist/js/select2.min.js"></script>

    <script>
    $(function () {
        $(document).ready(function() {
            //start

            /* Start Delete Button */
            $('body').on('click', '.sa-btn-delete', function(e){
                e.preventDefault()
                var btnForm = $(this)
                console.log(e)
                swal({
                    title: "@lang('crud.are_you_sure')",
                    text: '@lang("crud.you_will_delete") "' + btnForm.data("item") + '" @lang("crud.record")!',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "@lang('crud.yes_delete')",
                    cancelButtonText: "@lang('crud.no_delete')",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm){
                    if (isConfirm) {
                        swal("@lang('crud.deleted')", "@lang('crud.record_deleted')", "success");
                        btnForm.closest('form').submit()
                        console.log($(this))
                    } else {
                        swal("@lang('crud.cancelled')", "@lang('crud.record_safe')", "error");
                    }
                })
            })
            /* End Delete Button */

            // End
        })
    })
    </script>
    @stack('scripts')
</body>

</html>
