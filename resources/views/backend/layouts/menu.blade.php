
<li class="{{ Request::is('backend/users*') ? 'active' : '' }}">
    <a href="{{ route('backend.users.index') }}"><i class="fa fa-user"></i><span>@lang('models/users.plural')</span></a>
</li>

<li class="{{ Request::is('backend/tags*') ? 'active' : '' }}">
    <a href="{{ route('backend.tags.index') }}"><i class="fa fa-tags"></i><span>@lang('models/tags.plural')</span></a>
</li>

<li class="{{ Request::is('backend/categories*') ? 'active' : '' }}">
    <a href="{{ route('backend.categories.index') }}"><i class="fa fa-folder"></i><span>@lang('models/categories.plural')</span></a>
</li>

<li class="{{ Request::is('backend/crews*') ? 'active' : '' }}">
    <a href="{{ route('backend.crews.index') }}"><i class="fa fa-users"></i><span>@lang('models/crews.plural')</span></a>
</li>


<li class="{{ Request::is('backend/movies*') ? 'active' : '' }}">
    <a href="{{ route('backend.movies.index') }}"><i class="fa fa-video-camera"></i><span>@lang('models/movies.plural')</span></a>
</li>

<li class="{{ Request::is('backend/movieFiles*') ? 'active' : '' }}">
    <a href="{{ route('backend.movieFiles.index') }}"><i class="fa fa-file-movie-o"></i><span>@lang('models/movie_files.plural')</span></a>
</li>

<li class="{{ Request::is('backend/reviews*') ? 'active' : '' }}">
    <a href="{{ route('backend.reviews.index') }}"><i class="fa fa-star-half-empty"></i><span>@lang('models/reviews.plural')</span></a>
</li>

<li class="{{ Request::is('backend/pages*') ? 'active' : '' }}">
    <a href="{{ route('backend.pages.index') }}"><i class="fa fa-file"></i><span>@lang('models/pages.plural')</span></a>
</li>

<li class="{{ Request::is('backend/subscribers*') ? 'active' : '' }}">
    <a href="{{ route('backend.subscribers.index') }}"><i class="fa fa-flash"></i><span>@lang('models/subscribers.plural')</span></a>
</li>

<li class="{{ Request::is('backend/roles*') ? 'active' : '' }}">
    <a href="{{ route('backend.roles.index') }}"><i class="fa fa-anchor"></i><span>@lang('models/roles.plural')</span></a>
</li>

<li class="{{ Request::is('backend/settings*') ? 'active' : '' }}">
    <a href="{{ route('backend.settings') }}"><i class="fa fa-cog"></i><span>@lang('site.settings')</span></a>
</li>

