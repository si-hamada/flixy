<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/users.fields.name').':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', __('models/users.fields.email').':') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', __('models/users.fields.password').':') !!}
    {!! Form::text('password', '', ['class' => 'form-control']) !!}
</div>

<!-- Password Confirmation -->
<div class="form-group col-sm-6">
    {!! Form::label('password_confirmation', __('models/users.fields.password_confirmation').':') !!}
    {!! Form::text('password_confirmation', null, ['class' => 'form-control']) !!}
</div>

<!-- Role Field -->
<div class="form-group col-sm-6">
    {!! Form::label('role', __('models/roles.singular')) !!}
    {!! Form::select('role', $roles, (isset($user) ? $user->roles->first()->id : null), ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('backend.users.index') }}" class="btn btn-default">@lang('crud.cancel')</a>
</div>
