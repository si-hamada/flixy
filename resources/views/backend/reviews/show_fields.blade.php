<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/reviews.fields.id').':') !!}
    <p>{{ $review->id }}</p>
</div>

<!-- Movie Id Field -->
<div class="form-group">
    {!! Form::label('movie_id', __('models/reviews.fields.movie_id').':') !!}
    <p>{{ $review->movie_id }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', __('models/reviews.fields.user_id').':') !!}
    <p>{{ $review->user_id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', __('models/reviews.fields.title').':') !!}
    <p>{{ $review->title }}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', __('models/reviews.fields.slug').':') !!}
    <p>{{ $review->slug }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/reviews.fields.description').':') !!}
    <p>{{ $review->description }}</p>
</div>

<!-- Rate Field -->
<div class="form-group">
    {!! Form::label('rate', __('models/reviews.fields.rate').':') !!}
    <p>{{ $review->rate }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/reviews.fields.created_at').':') !!}
    <p>{{ $review->created_at }}</p>
</div>

