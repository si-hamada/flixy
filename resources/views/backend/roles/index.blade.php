@extends('backend.layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">@lang('models/roles.plural')</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">@lang('site.home')</a></li>
                    <li class="breadcrumb-item active">@lang('models/roles.plural')</li>
                </ol>
                <a href="{{ route('backend.roles.create') }}" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> @lang('crud.add_new')</a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    @include('flash::message')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    @include('backend.roles.table')
                </div>
            </div>
        </div>
    </div>
@endsection

