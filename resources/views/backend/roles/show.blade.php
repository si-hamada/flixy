@extends('backend.layouts.app')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">@lang('models/roles.singular')</h4>
        </div>
        <div class="col-md-7 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('backend.home') }}">@lang('site.home')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('backend.roles.index') }}">@lang('models/roles.singular')</a></li>
                    <li class="breadcrumb-item active">@lang('crud.detail')</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    @include('backend.roles.show_fields')
                    <a href="{{ route('backend.roles.index') }}" class="btn btn-default">
                        @lang('crud.back')
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
