<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', __('models/roles.fields.id').':') !!}
    <p>{{ $role->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/roles.fields.name').':') !!}
    <p>{{ $role->name }}</p>
</div>

<!-- Display Name Field -->
<div class="form-group">
    {!! Form::label('display_name', __('models/roles.fields.display_name').':') !!}
    <p>{{ $role->display_name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', __('models/roles.fields.description').':') !!}
    <p>{{ $role->description }}</p>
</div>

<!-- Permissions Field -->
<div class="form-group">
    {!! Form::label('description', __('models/roles.fields.description').':') !!}
    <p>
        @foreach ($role->permissions as $item)
            <span class="btn btn-sm fc btn-primary m-1">{{ $item['name'] }}</span>
        @endforeach
    </p>
</div>

