@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else
        @push('scripts')
        <script>
            $.toast({
                    closeButton: true,
                    heading: "{{ $message['message'] }}",
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: "{{ $message['level'] == 'danger' ? 'error' : $message['level'] }}",
                    hideAfter: false,
                    timeOut: 0,
                    extendedTimeOut: 0,
                    stack: 6
                });
        </script>
        @endpush
    @endif
@endforeach

{{ session()->forget('flash_notification') }}
