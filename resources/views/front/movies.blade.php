@extends('front.layouts.app')
@section('title', __('site.movies_listing'))

@section('content')

<div class="hero common-hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1> @lang('site.movies_listing') </h1>
					<ul class="breadcumb">
						<li class="active"><a href="{{ url('/') }}">@lang('site.home')</a></li>
						<li> <span class="ion-ios-arrow-right"></span> @lang('site.movies_listing')</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="page-single">
	<div class="container">
		<div class="row ipad-width">
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="topbar-filter">
					<p>@lang('site.found') <span>{{ $movies->total() }} @lang('site.movies')</span> @lang('site.in_total')</p>
					<label>@lang('site.sort_by'):</label>
					<select onChange="window.location.href=this.value">
						<option value="">@lang('site.sort_by')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'rate' ,'sortDir' => 'asc']) }}">@lang('site.rating_ascending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'rate' ,'sortDir' => 'desc']) }}">@lang('site.rating_descending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'year' ,'sortDir' => 'asc']) }}">@lang('site.date_ascending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'year' ,'sortDir' => 'desc']) }}">@lang('site.date_descending')</option>
					</select>
					{{-- <a href="movielist.html" class="list"><i class="ion-ios-list-outline "></i></a>
					<a  href="moviegrid.html" class="grid"><i class="ion-grid active"></i></a> --}}
				</div>
				<div class="flex-wrap-movielist">

                    @foreach ($movies as $item)
                        <div class="movie-item-style-2 movie-item-style-1">
                            <img src="{{ $item->poster }}" alt="{{ $item->name }}">
                            <div class="hvr-inner">
                                <a  href="{{ route('movies.single', $item->slug) }}"> @lang('site.read_more') <i class="ion-android-arrow-dropright"></i> </a>
                            </div>
                            <div class="mv-item-infor">
                                <h6><a href="{{ route('movies.single', $item->slug) }}">{{ $item->name }} ({{ $item->year }})</a></h6>
                                <p class="rate"><i class="ion-android-star"></i><span>{{ $item->rate }}</span> /10</p>
                            </div>
                        </div>
                    @endforeach

				</div>
				<div class="topbar-filter">
                    <label>@lang('site.movies') @lang('site.per_page'):</label>
                    <p>{{ $movies->perPage() }}</p>
					{{-- <select>
						<option value="range">20 Movies</option>
						<option value="saab">10 Movies</option>
					</select> --}}

					<div class="pagination2">
                        <span>@lang('site.page') {{ $movies->currentPage() }} @lang('site.of') {{ $movies->lastPage() }}:</span>
                        {{ $movies->links() }}
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="sidebar">
                    <!-- Find Us on Movie Search -->
					@include('front.layouts.movie_search')
                    <!-- Find Us on Movie Search -->
                    <!-- Find Us on Ads -->
					@include('front.layouts.ads')
                    <!-- Find Us on Ads -->
                    <!-- Find Us on FB -->
					@include('front.layouts.fb')
                    <!-- Find Us on FB -->
                    <!-- Find Us on TW -->
					@include('front.layouts.tw')
                    <!-- Find Us on TW -->
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
