<!-- start | Slider -->
<div class="slider movie-items">
    <div class="container">
		<div class="row">
			<div class="social-link">
				<p>@lang('site.follow_us'): </p>
				<a target="_blank" href="{{ settings()->get('facebook_link') }}"><i class="ion-social-facebook"></i></a>
				<a target="_blank" href="{{ settings()->get('twitter_link') }}"><i class="ion-social-twitter"></i></a>
				<a target="_blank" href="{{ settings()->get('google_link') }}"><i class="ion-social-googleplus"></i></a>
				<a target="_blank" href="{{ settings()->get('youtube_link') }}"><i class="ion-social-youtube"></i></a>
            </div>
	    	<div class="slick-multiItemSlider">
                @foreach ($movies as $item)
                    <div class="movie-item">
                        <div class="mv-img">
                            <a href="{{ $item->poster }}"><img src="{{ $item->poster }}" alt="{{ $item->name }}" width="285" height="437"></a>
                        </div>
                        <div class="title-in">
                            <div class="cate">
                                <span class="{{ \Arr::random(['orange', 'green', 'yell', 'blue']) }}"><a href="{{ route('movies', 'category=' . $item->category->id) }}">{{ $item->category->name }}</a></span>
                            </div>
                            <h6><a href="{{ route('movies.single', $item->slug) }}">{{ $item->name }}</a></h6>
                            <p><i class="ion-android-star"></i><span>{{ $item->rate }}</span> /10</p>
                        </div>
                    </div>
                @endforeach
	    	</div>
	    </div>
	</div>
</div>
<!-- end | Slider -->
