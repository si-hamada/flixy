<div class="searh-form">
    <h4 class="sb-title">@lang('site.search_for_movie')</h4>
    <form class="form-style-1" action="{{ url()->full() }}">
        <div class="row">
            <div class="col-md-12 form-it">
                <label>@lang('site.movie') @lang('site.name')</label>
                {!! Form::text('search', null, ['placeholder' => __('site.enter_keywords')]) !!}
            </div>
            <div class="col-md-12 form-it">
                <label>@lang('site.type')</label>
                <div class="group-ip">
                    {!! Form::select('type', ['all' => __('site.all'), 'movies' => __('site.movies'), 'series' => __('site.serieses')], null, ['class' => 'ui fluid dropdown']) !!}
                </div>
            </div>
            <div class="col-md-12 form-it">
                <label>@lang('models/categories.singular')</label>
                <div class="group-ip">
                    {!! Form::select('category', $categories, null, ['class' => 'ui fluid dropdown', 'placeholder' => __('site.please_select_category')]) !!}
                </div>
            </div>
            <div class="col-md-12 form-it">
                <label>@lang('models/tags.plural')</label>
                <div class="group-ip">
                    {!! Form::select('tags[]', $tags, null, ['class' => 'ui fluid dropdown', 'multiple' => '', 'placeholder' => __('site.please_select_tags')]) !!}
                </div>
            </div>
            <div class="col-md-12 form-it">
                <label>@lang('site.release') @lang('site.year')</label>
                {!! Form::number('year') !!}
            </div>
            <div class="col-md-12 form-it">
                <label>@lang('site.rating') @lang('site.range')</label>
                <div class="row">
                    <div class="col-md-6">
                        {!! Form::number('rate_from') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::number('rate_to') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-12 ">
                <input class="submit" type="submit" value="@lang('site.submit')">
            </div>
        </div>
    </form>
</div>
