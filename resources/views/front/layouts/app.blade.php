<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">


<head>
	<!-- Basic need -->
	<title>@yield('title') - {{ config('app.name') }}</title>
	<meta charset="UTF-8">
	<!-- favicon, cards, tiles, icons -->
    <meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
    <link rel="profile" href="#">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('backend-files') }}/assets/images/favicon.png">
    <meta name="msapplication-square70x70logo" content="{{ asset('backend-files') }}/assets/images/favicon.png" />
    <meta name="msapplication-square150x150logo" content="{{ asset('backend-files') }}/assets/images/favicon.png" />
    <meta name="msapplication-wide310x150logo" content="{{ asset('backend-files') }}/assets/images/favicon.png" />
    <meta name="msapplication-square310x310logo" content="{{ asset('backend-files') }}/assets/images/favicon.png" />
    <link rel="apple-touch-icon-precomposed" href="{{ asset('backend-files') }}/assets/images/favicon.png" />
    <!-- end favicon -->

    <!-- facebook open graph -->
    <meta property="og:url"                content="" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="" />
    <meta property="og:description"        content="" />
    <meta property="og:image"              content="" />
    <!-- end facebook open graph -->

    <!-- Schema MicroData (Google+,Google, Yahoo, Bing,) -->
    <meta itemprop="name"  content="" />
    <meta itemprop="url" content="" />
    <meta itemprop="author" content="" />
    <meta itemprop="image" content=""  />
    <meta itemprop="description" content=""
    />
    <!-- End Schema MicroData -->

    <!-- twitter cards -->
    <meta name="twitter:image" content="">
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="" />
    <meta name="twitter:creator" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image:src" content="" />
    <meta name="twitter:description" content="" />
    <!-- end twitter cards -->

    <!--Google Font-->
    <link rel="stylesheet" href='https://fonts.googleapis.com/css?family=Dosis:400,700,500|Nunito:300,400,600' />
	<!-- Mobile specific meta -->
	<meta name=viewport content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone-no">

	<!-- CSS files -->
	<link rel="stylesheet" href="{{ asset('front') }}/css/easy-autocomplete.min.css">
	<link rel="stylesheet" href="{{ asset('front') }}/css/easy-autocomplete.themes.min.css">
	<link rel="stylesheet" href="{{ asset('front') }}/css/plugins.css">
	<link rel="stylesheet" href="{{ asset('front') }}/css/style.css">

</head>
<body>
<!--preloading-->
<div id="preloader">
    <img class="logo" src="{{ asset('front') }}/images/logo1.png" alt="" width="119" height="58">
    <div id="status">
        <span></span>
        <span></span>
    </div>
</div>
<!--end of preloading-->
<!--login form popup-->
<div class="login-wrapper" id="login-content">
    <div class="login-content">
        <a href="#" class="close">x</a>
        <h3>@lang('auth.sign_in')</h3>
        <form id="log-form" method="post" action="{{ url('/login') }}">
            @csrf
            <div class="row">
                <label for="email">
                    @lang('auth.email'):
                    <input type="text" name="email" id="email" placeholder="example@email.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required="required" />
                </label>
            </div>

            <div class="row">
                <label for="password">
                    @lang('auth.password'):
                    <input type="password" name="password" id="password" placeholder="******" pattern=".{6,}" required="required" />
                </label>
            </div>
            <div class="row form-group">
                <div class="remember">
					<div>
                        <input type="checkbox" name="remember" id="rememberme" value="Remember me">
                        <label class="custom-control-label" for="rememberme" style="display: inline;vertical-align: top;">@lang('auth.remember_me')</label>
					</div>
                    <a href="{{ url('/login') }}">@lang('auth.login.forgot_password')</a>
                </div>
            </div>
            <div class="row">
                <button type="submit">@lang('auth.sign_in')</button>
            </div>
        </form>
        <div class="row">
            <p>@lang('auth.or_via_social')</p>
            <div class="social-btn-2">
                <a class="fb" href="{{ url('/login/facebook') }}"><i class="ion-social-facebook"></i>Facebook</a>
                <a class="go" href="{{ url('/login/google') }}"><i class="ion-social-google"></i>Google</a>
            </div>
        </div>
    </div>
</div>
<!--end of login form popup-->
<!--signup form popup-->
<div class="login-wrapper"  id="signup-content">
    <div class="login-content">
        <a href="#" class="close">x</a>
        <h3>@lang('auth.register')</h3>
        <form id="reg-form" method="post" action="{{ url('/register') }}">
            @csrf
            <div class="row">
                <label for="username-2">
                    @lang('auth.full_name'):
                    <input type="text" name="username" id="username-2" placeholder="Hugh Jackman" pattern="^[a-zA-Z 0-9-_\.]{8,20}$" required="required" />
                </label>
            </div>

            <div class="row">
                <label for="email-2">
                    @lang('auth.email'):
                    <input type="text" name="email" id="email" placeholder="example@email.com" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required="required" />
                </label>
            </div>
            <div class="row">
                <label for="password-2">
                    @lang('auth.password'):
                    <input type="password" name="password" id="password-2" placeholder="" pattern="(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required="required" />
                </label>
            </div>
            <div class="row">
                <label for="repassword-2">
                    @lang('auth.confirm_password'):
                    <input type="password" name="password_confirmation" id="repassword-2" placeholder="" pattern="(?=^.{6,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required="required" />
                </label>
            </div>
            <div class="row">
                <button type="submit">@lang('auth.register')</button>
            </div>
        </form>
    </div>
</div>
<!--end of signup form popup-->

<!-- BEGIN | Header -->
<header class="ht-header">
	<div class="container">
		<nav class="navbar navbar-default navbar-custom">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header logo">
                    <div class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">@lang('site.toggle_navigation')</span>
                        <div id="nav-icon1">
							<span></span>
							<span></span>
							<span></span>
						</div>
                    </div>
                    <a href="{{ url('/') }}"><img class="logo" src="{{ asset('front') }}/images/logo1.png" alt="" width="119" height="58"></a>
                </div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse flex-parent" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav flex-child-menu menu-left">
						<li class="hidden">
							<a href="#page-top"></a>
						</li>
						<li class="dropdown first">
							<a href="{{ url('/') }}" class="btn btn-default dropdown-toggle lv1">
							@lang('site.home')
							</a>
						</li>
						<li class="dropdown first">
							<a class="btn btn-default dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
							@lang('site.movies') <i class="fa fa-angle-down" aria-hidden="true"></i>
							</a>
							<ul class="dropdown-menu level1">
								<li><a href="{{ route('movies') }}">@lang('site.all')</a></li>
								<li><a href="{{ route('movies', 'type=Movie') }}">@lang('site.movies')</a></li>
								<li class="it-last"><a href="{{ route('movies', 'type=Series') }}">@lang('site.serieses')</a></li>
							</ul>
						</li>
						<li class="dropdown first">
							<a href="{{ route('celebrities') }}" class="btn btn-default dropdown-toggle lv1">
                                @lang('site.celebrities')
							</a>
						</li>
						<li class="dropdown first">
							<a class="btn btn-default dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
							@lang('site.news') <i class="fa fa-angle-down" aria-hidden="true"></i>
							</a>
							<ul class="dropdown-menu level1">
								<li><a href="javascript:;">blog List</a></li>
								<li><a href="javascript:;">blog Grid</a></li>
								<li class="it-last"><a href="javascript:;">blog Detail</a></li>
							</ul>
						</li>
						<li class="dropdown first">
							<a class="btn btn-default dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
							community <i class="fa fa-angle-down" aria-hidden="true"></i>
							</a>
							<ul class="dropdown-menu level1">
								<li><a href="javascript:;">user favorite grid</a></li>
								<li><a href="javascript:;">user favorite list</a></li>
								<li><a href="javascript:;">user profile</a></li>
								<li class="it-last"><a href="javascript:;">user rate</a></li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav flex-child-menu menu-right">
                        @auth
                            <li class="dropdown first">
                                <a class="btn btn-default dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
                                    <img src="{{ auth()->user()->image }}" width="30" alt="{{ auth()->user()->name }}">
                                    @lang('site.hi') {{ auth()->user()->name }}! <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </a>
                                <ul class="dropdown-menu level1">
                                    <li><a href="{{ route('profile') }}">@lang('site.profile')</a></li>
                                    <li><a href="{{ route('user_movies', 'faved') }}">@lang('site.Favorite movies')</a></li>
                                    <li><a href="{{ route('user_movies', 'later') }}">@lang('site.Saved movies')</a></li>
                                    <li><a href="{{ route('user_movies', 'watched') }}">@lang('site.Watched movies')</a></li>
                                    {{-- <li><a href="javascript:;">user rate</a></li> --}}
                                    @role('super_admin|')
                                        <li><a target="_blank" href="{{ route('backend.home') }}">@lang('site.dashboard')</a></li>
                                    @endrole
                                    <li class="it-last">
                                        <!-- Logout -->
                                        <a href="{{ url('/logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> @lang('auth.sign_out')</a>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;"> @csrf </form>
                                        <!-- Logout -->
                                    </li>
                                </ul>
                            </li>
                        @else
                            <li class="loginLink-x"><a href="{{ route('login') }}">LOG In</a></li>
                            <li class="btn signupLink-x"><a href="{{ url('register') }}">sign up</a></li>
                        @endauth
					</ul>
				</div>
			<!-- /.navbar-collapse -->
        </nav>

        <!-- top search form -->
        <form id="main_search" action="{{ route('mainSearch') }}">
            <div class="top-search">
                <select name="type">
                    <option value="">@lang('site.all')</option>
                    <option value="movie">@lang('site.movies')</option>
                    <option value="series">@lang('site.serieses')</option>
                </select>
                <input name="search" type="text" placeholder="Search for a movie, TV Show or celebrity that you are looking for">
            </div>
        </form>
	</div>
</header>
<!-- END | Header -->

@yield('content')

<!-- footer section-->
<footer class="ht-footer">
	<div class="container">
		<div class="flex-parent-ft">
			<div class="flex-child-ft item1">
                <a href="index-javascript:;"><img class="logo" src="{{ asset('front') }}/images/logo1.png" alt=""></a>
                <p>5th Avenue st, manhattan<br>
				New York, NY 10001</p>
                <p>Call us: <a href="#">(+01) 202 342 6789</a></p>
                <div class="social-link">
                    <a target="_blank" href="{{ settings()->get('facebook_link') }}"><i class="ion-social-facebook"></i></a>
                    <a target="_blank" href="{{ settings()->get('twitter_link') }}"><i class="ion-social-twitter"></i></a>
                    <a target="_blank" href="{{ settings()->get('google_link') }}"><i class="ion-social-googleplus"></i></a>
                    <a target="_blank" href="{{ settings()->get('youtube_link') }}"><i class="ion-social-youtube"></i></a>
                </div>
			</div>
			<div class="flex-child-ft item2">
				<h4>Resources</h4>
				<ul>
					<li><a href="#">About</a></li>
					<li><a href="#">Blockbuster</a></li>
					<li><a href="#">Contact Us</a></li>
					<li><a href="#">Forums</a></li>
					<li><a href="#">Blog</a></li>
					<li><a href="#">Help Center</a></li>
				</ul>
			</div>
			<div class="flex-child-ft item3">
				<h4>Legal</h4>
				<ul>
					<li><a href="#">Terms of Use</a></li>
					<li><a href="#">Privacy Policy</a></li>
					<li><a href="#">Security</a></li>
				</ul>
			</div>
			<div class="flex-child-ft item4">
				<h4>Account</h4>
				<ul>
					<li><a href="#">My Account</a></li>
					<li><a href="#">Watchlist</a></li>
					<li><a href="#">Collections</a></li>
					<li><a href="#">User Guide</a></li>
				</ul>
			</div>
			<div class="flex-child-ft item5">
				<h4>@lang('site.newsletter')</h4>
				<p>Subscribe to our newsletter system now <br> to get latest news from us.</p>
				<form action="#">
					<input type="text" placeholder="Enter your email...">
				</form>
				<a href="#" class="btn">Subscribe now <i class="ion-ios-arrow-forward"></i></a>
			</div>
		</div>
	</div>
	<div class="ft-copyright">
		<div class="ft-left">
			<p><a target="_blank" href="https://www.templateshub.net">Templates Hub</a></p>
		</div>
		<div class="backtotop">
			<p><a href="#" id="back-to-top">Back to top  <i class="ion-ios-arrow-thin-up"></i></a></p>
		</div>
	</div>
</footer>
<!-- end of footer section-->

<script src="{{ asset('front') }}/js/jquery.js"></script>
<script src="{{ asset('front') }}/js/plugins.js"></script>
<script src="{{ asset('front') }}/js/plugins2.js"></script>
<script src="{{ asset('front') }}/js/jquery.easy-autocomplete.min.js"></script>
<script src="{{ asset('front') }}/js/playerjs.js"></script>
<script src="{{ asset('front') }}/js/custom.js"></script>

<script>
    let options = {
        source:function (request, response) {
            setTimeout(
                function() {
                    response(shouldComplete ? availableTags : null);
                },
                2000);
        },
        url: function() {
            return "{{ route('mainSearch') }}?" + $("#main_search").serialize();
        },
        getValue: "name",
        template: {
            type: "iconLeft",
            fields: {
                iconSrc: "poster"
            }
        },
        list: {
            onChooseEvent: function () {
                let item = $('#main_search').getSelectedItemData();
                if (item.birthday){
                    window.location.href = '{{ route("celebrities") }}/' + item.slug;
                } else {
                    window.location.href = '{{ route("movies") }}/' + item.slug;
                }
            }
        },
        theme: "blue"
    };

    $("#main_search").easyAutocomplete(options);

	//to movie rate js
	let tomovierate = $('#to-movie-rate');
    tomovierate.on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $('#movie-rate').offset().top - 99
        }, 700);
    });//end to movie rate js

	//rate stars js
	$('body').on('mouseover', '.rate-stars', function (e) {
        let num = $(this).data('num')
        $('.rate-stars').each(function(index){
            if ($(this).data('num') - 1 < num){
                $(this).attr('class', 'rate-stars ion-ios-star')
            } else {
                $(this).attr('class', 'rate-stars ion-ios-star-outline')
            }
        })
    });
	$('body').on('mouseleave', '.rate-stars', function (e) {
        $('.rate-stars').each(function(index){
            if ($(this).data('num') - 1 < $(this).data('rate')){
                $(this).attr('class', 'rate-stars ion-ios-star')
            } else {
                $(this).attr('class', 'rate-stars ion-ios-star-outline')
            }
        })
    });//end rate stars js

    //pop up for review form
    $('body').on('click', '.rate-stars', function(event){
        event.preventDefault();
        let rating = $(this).data('num');
        $('#review-content .rate-stars').data('rate', rating)
        $('#review-content input[name="rate"]').val(rating)
        $('#review-content').parents($(".overlay")).addClass("openform");
		$(document).on('click', function(e){
		let target = $(e.target);
		if ($(target).hasClass("overlay")){
				$(target).find($('#review-content')).each( function(){
					$(this).removeClass("openform");
				});
				setTimeout( function(){
					$(target).removeClass("openform");
				}, 350);
			}
		});
    });//end pop up for review form

    //end of on click fav icon
    $(document).on('click', '.social-btn .add-list', function () {

        let that = $(this);
        let url = that.data('url');

        $.ajax({
            url: url,
            method: 'GET',
            success: function (response) {
                that.find('i[class^="ion"]').toggleClass('saved')
            },
            error: function (error) {
                // console.log(error)
                alert("@lang('site.log_first')")
            }
        });//end of ajax call

    });//end of on click fav icon

</script>

@stack('scripts')

</body>
</html>
