<div class="searh-form">
    <h4 class="sb-title">@lang('site.search_celebrity')</h4>
    <form class="form-style-1 celebrity-form" action="{{ url()->full() }}">
        <div class="row">
            <div class="col-md-12 form-it">
                <label>@lang('site.celebrity_name')</label>
                {!! Form::text('search', null, ['placeholder' => __('site.enter_keywords')]) !!}
            </div>
            <div class="col-md-12 form-it">
                <label>@lang('models/tags.plural')</label>
                <div class="group-ip">
                    {!! Form::select('tags[]', $tags, null, ['class' => 'ui fluid dropdown', 'multiple' => '', 'placeholder' => __('site.please_select_tags')]) !!}
                </div>
            </div>
            <div class="col-md-12 form-it">
                <label>@lang('site.year_of_birth')</label>
                <div class="row">
                    <div class="col-md-6">
                        {!! Form::number('year_from') !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::number('year_to') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-12 ">
                <input class="submit" type="submit" value="@lang('site.submit')">
            </div>
        </div>
    </form>
</div>
