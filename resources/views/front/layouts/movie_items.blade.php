<!-- start | Movie Items -->
<div class="movie-items">
	<div class="container">
		<div class="row ipad-width">
			<div class="col-md-8">
				<div class="title-hd">
					<h2>#@lang('site.newest')</h2>
					<a href="{{ route('movies') }}" class="viewall">@lang('site.view_all') <i class="ion-ios-arrow-right"></i></a>
				</div>
                <div id="tab1" class="tab active">
                    <div class="row">
                        <div class="slick-multiItem">
                            @foreach ($movies as $item)
                                <div class="slide-it">
                                    <div class="movie-item">
                                        <div class="mv-img">
                                            <img src="{{ $item->poster }}" alt="{{ $item->name }}" width="185" height="284">
                                        </div>
                                        <div class="hvr-inner">
                                            <a  href="{{ route('movies.single', $item->slug) }}"> @lang('site.read_more') <i class="ion-android-arrow-dropright"></i> </a>
                                        </div>
                                        <div class="title-in">
                                            <h6><a href="{{ route('movies.single', $item->slug) }}">{{ $item->name }}</a></h6>
                                            <p><i class="ion-android-star"></i><span>{{ $item->rate }}</span> /10</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
				<div class="title-hd">
					<h2>#@lang('site.popular')</h2>
					<a href="{{ route('movies') }}" class="viewall">@lang('site.view_all') <i class="ion-ios-arrow-right"></i></a>
				</div>
                <div id="tab21" class="tab">
                    <div class="row">
                        <div class="slick-multiItem">
                            @foreach ($movies as $item)
                                <div class="slide-it">
                                    <div class="movie-item">
                                        <div class="mv-img">
                                            <img src="{{ $item->poster }}" alt="{{ $item->name }}" width="185" height="284">
                                        </div>
                                        <div class="hvr-inner">
                                            <a  href="{{ route('movies.single', $item->slug) }}"> @lang('site.read_more') <i class="ion-android-arrow-dropright"></i> </a>
                                        </div>
                                        <div class="title-in">
                                            <h6><a href="{{ route('movies.single', $item->slug) }}">{{ $item->name }}</a></h6>
                                            <p><i class="ion-android-star"></i><span>{{ $item->rate }}</span> /10</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-md-4">
				<div class="sidebar">
					<div class="ads">
						<img src="{{ asset('front') }}/images/uploads/ads1.png" alt="" width="336" height="296">
					</div>
					<div class="celebrities">
                        <h4 class="sb-title">@lang('site.spotlight_celebrities')</h4>
                        @foreach ($celebrities as $item)
                            <div class="celeb-item">
                                <a href="{{ route('celebrities.single', $item->slug) }}"><img src="{{ $item->image }}" alt="" width="70" height="70"></a>
                                <div class="celeb-author">
                                    <h6><a href="#">{{ $item->name }}</a></h6>
                                    <span>{{ explode('+', $item->position)[0] }}</span>
                                </div>
                            </div>
                        @endforeach
						<a href="{{ route('celebrities') }}" class="btn">@lang('site.see_all') @lang('site.celebrities')<i class="ion-ios-arrow-right"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end | Movie Items -->
