@extends('front.layouts.app')
@section('title', __('site.home'))

@section('content')
<!-- start | Slider -->
@include('front.layouts.slider')
<!-- end | Slider -->

<!-- start | Movie Items -->
@include('front.layouts.movie_items')
<!-- end | Movie Items -->

<!-- start | Movie Items -->
@include('front.layouts.trailers')
<!-- end | Movie Items -->

<!-- start | Latest News -->
{{-- @include('front.layouts.latestnews') --}}
<!-- end | Latest News -->
@endsection
