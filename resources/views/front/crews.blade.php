@extends('front.layouts.app')
@section('title', __('site.celebrities'))

@section('content')

<div class="hero common-hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1> @lang('site.celebrities_listing') </h1>
					<ul class="breadcumb">
						<li class="active"><a href="{{ url('/') }}">@lang('site.home')</a></li>
						<li> <span class="ion-ios-arrow-right"></span> @lang('site.celebrities')</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- celebrity list section-->
<div class="page-single">
	<div class="container">
		<div class="row ipad-width2">
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="topbar-filter">
                    <p>@lang('site.found') <span>{{ $crews->total() }} @lang('site.celebrities')</span> @lang('site.in_total')</p>
					<label>@lang('site.sort_by'):</label>
					<select onChange="window.location.href=this.value">
						<option value="">@lang('site.sort_by')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'name' ,'sortDir' => 'asc']) }}">@lang('site.name_ascending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'name' ,'sortDir' => 'desc']) }}">@lang('site.name_descending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'birthday' ,'sortDir' => 'asc']) }}">@lang('site.birth_ascending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'birthday' ,'sortDir' => 'desc']) }}">@lang('site.birth_descending')</option>
					</select>
				</div>
				<div class="row">
					@foreach ($crews as $crew)
						<div class="col-md-12">
							<div class="ceb-item-style-2">
								<img width="90" height="125" src="{{ $crew->image }}" alt="{{ $crew->name }}">
								<div class="ceb-infor">
									<h2><a href="{{ route('celebrities.single', $crew->slug) }}">{{ $crew->name }}</a></h2>
									<span>{!! explode('+', $crew->position)[0] !!}, {{ $crew->country }}</span>
                                    <p>{!! Str::limit($crew->bio, 210, ' ...') !!}</p>
								</div>
							</div>
						</div>
					@endforeach
				</div>
				<div class="topbar-filter">
					<label>@lang('site.celebrities') @lang('site.per_page'):</label>
                    <p>{{ $crews->perPage() }}</p>

					<div class="pagination2">
                        <span>@lang('site.page') {{ $crews->currentPage() }} @lang('site.of') {{ $crews->lastPage() }}:</span>
                        {{ $crews->links() }}
					</div>
				</div>
			</div>
			<div class="col-md-3 col-xs-12 col-sm-12">
				<div class="sidebar">
					<!-- Find Us on Celebrities Search -->
					@include('front.layouts.celebrities_search')
                    <!-- Find Us on Celebrities Search -->
                    <!-- Find Us on Ads -->
					@include('front.layouts.ads')
                    <!-- Find Us on Ads -->
                    <!-- Find Us on FB -->
					@include('front.layouts.fb')
                    <!-- Find Us on FB -->
                    <!-- Find Us on TW -->
					@include('front.layouts.tw')
                    <!-- Find Us on TW -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end of celebrity list section-->

@endsection
