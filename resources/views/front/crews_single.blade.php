@extends('front.layouts.app')
@section('title', $crew->name)

@section('content')

<div class="hero hero3" style="background: url({{ asset('front/images/uploads/celeb-hero-single.jpg') }}) no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- <h1> movie listing - list</h1>
				<ul class="breadcumb">
					<li class="active"><a href="#">Home</a></li>
					<li> <span class="ion-ios-arrow-right"></span> movie listing</li>
				</ul> -->
            </div>
        </div>
    </div>
</div>
<!-- celebrity single section-->
<div class="page-single movie-single cebleb-single">
	<div class="container">
		<div class="row ipad-width">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="mv-ceb">
					<img src="{{ $crew->image }}" alt="">
				</div>
			</div>
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="movie-single-ct">
					<h1 class="bd-hd">{{ $crew->name }}</h1>
					<p class="ceb-single">{{ str_replace('+', ' | ', $crew->position) }}</p>
					<div class="social-link cebsingle-socail">
						<a href="https://www.facebook.com/sharer.php?u={{ url()->full() }}"><i class="ion-social-facebook"></i></a>
						<a href="https://www.twitter.com/share?url={{ url()->full() }}"><i class="ion-social-twitter"></i></a>
						{{-- <a href="#"><i class="ion-social-googleplus"></i></a>
						<a href="#"><i class="ion-social-linkedin"></i></a> --}}
					</div>
					<div class="movie-tabs">
						<div class="tabs">
							<ul class="tab-links tabs-mv">
								<li class="active"><a href="#overviewceb">@lang('site.overview')</a></li>
								<li><a href="#biography"> @lang('site.biography')</a></li>
								<li><a href="#mediaceb"> @lang('site.media')</a></li>
								<li><a href="#filmography">@lang('site.filmography')</a></li>
							</ul>
                            <div class="tab-content">
                                <div id="overviewceb" class="tab active">
                                    <div class="row">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <p>{!! Str::limit($crew->bio, 210, ' ...') !!}</p>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12">
                                            <div class="sb-it">
                                                <h6>@lang('site.fullname'):  </h6>
                                                <p><a>{{ $crew->name }}</a></p>
                                            </div>
                                            <div class="sb-it">
                                                <h6>@lang('site.date_of_birth'): </h6>
                                                <p>{{ $crew->birthday }}</p>
                                            </div>
                                            <div class="sb-it">
                                                <h6>@lang('site.country'):  </h6>
                                                <p>{{ $crew->country }}</p>
                                            </div>
                                            <div class="sb-it">
                                                <h6>@lang('site.keywords'):</h6>
                                                <p class="tags">
                                                    @foreach ($crew->tags as $tag)
                                                        <span class="time"><a href="{{ route('celebrities', 'tags[]='.$tag->id) }}">{{ $tag->name }}</a></span>
                                                    @endforeach
                                                </p>
                                            </div>
                                            <div class="ads">
                                                <img src="{{ asset('front/images/uploads/ads1.png') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="biography" class="tab">
                                <div class="row">
                                        <div class="rv-hd">
                                            <div>
                                                <h3>@lang('site.biography') @lang('site.of')</h3>
                                                <h2>{{ $crew->name }}</h2>
                                            </div>
                                        </div>
                                        <p>{{ $crew->bio }}</p>
                                    </div>
                                </div>
                                <div id="mediaceb" class="tab">
                                    <div class="row">
                                        <div class="rv-hd">
                                            <div>
                                                <h3>@lang('site.media') @lang('site.of')</h3>
                                                <h2>{{ $crew->name }}</h2>
                                            </div>
                                        </div>
                                        <div class="title-hd-sm">
                                            <h4>@lang('site.media') <span>({{ $crew->gallery->count() }})</span></h4>
                                        </div>
                                        <div class="mvsingle-item">
                                            @forelse ($crew->gallery as $file)
                                                <a class="img-lightbox" data-fancybox-group="gallery" href="{{ $file->getFullUrl() }}"><img style="height: 100px;width: 100px;" src="{{ $file->getFullUrl() }}" alt="{{ $crew->name }}"></a>
                                            @empty
                                                <h4>@lang('site.no_data_found')</h4>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                                <div id="filmography" class="tab">
                                    <div class="row">
                                        <div class="rv-hd">
                                            <div>
                                                <h3>@lang('site.filmography') @lang('site.of')</h3>
                                                <h2>{{ $crew->name }}</h2>
                                            </div>
                                        </div>
                                        <!-- movie cast -->
                                        <div class="mvcast-item">
                                            @forelse ($crew->movies as $item)
                                                <div class="cast-it">
                                                    <div class="cast-left cebleb-film">
                                                        <img width="55" height="77" src="{{ $item->poster }}" alt="{{ $item->name }}">
                                                        <div>
                                                            <a href="#">{{ $item->name }} </a>
                                                            <p class="time">{{ $item->pivot->crewAs }}</p>
                                                        </div>

                                                    </div>
                                                    <p>...  {{ $item->year }}</p>
                                                </div>
                                            @empty
                                                <h4>@lang('site.no_data_found')</h4>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
<!-- celebrity single section-->

@endsection
