@extends('front.layouts.app')
@section('title', auth()->user()->name)

@section('content')

<div class="hero user-hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1>{{ auth()->user()->name }}’s @lang('site.profile')</h1>
					<ul class="breadcumb">
						<li class="active"><a href="{{ url('/') }}">@lang('site.home')</a></li>
						<li> <span class="ion-ios-arrow-right"></span>@lang('site.profile')</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- profile section-->
<div class="page-single">
    <div class="container">
        <div class="row ipad-width">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="user-information">
                    <div class="user-img">
                        <a href="#"><img src="{{ auth()->user()->image }}" alt=""><br></a>
                        {{-- <a href="#" class="redbtn">@lang('site.Change avatar')</a> --}}
                    </div>
                    <div class="user-fav">
                        <p>@lang('site.Account Details')</p>
                        <ul>
                            <li class="active"><a href="{{ route('profile') }}">@lang('site.profile')</a></li>
                            <li><a href="{{ route('user_movies', 'faved') }}">@lang('site.Favorite movies')</a></li>
                            <li><a href="{{ route('user_movies', 'later') }}">@lang('site.Saved movies')</a></li>
                            <li><a href="{{ route('user_movies', 'watched') }}">@lang('site.Watched movies')</a></li>
                            {{-- <li><a href="userrate.html">@lang('site.Rated movies')</a></li> --}}
                        </ul>
                    </div>
                    <div class="user-fav">
                        <p>@lang('site.Others')</p>
                        <ul>
                            <li><a href="{{ route('profile') }}">@lang('site.Change password')</a></li>
                            <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('site.Log out')</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="form-style-1 user-pro" action="#">
                    @include('adminlte-templates::common.errors')
                    <form action="{{ url()->full() }}" method="POST" class="user">
                        @csrf
                        <h4>01. @lang('site.Profile details')</h4>
                        <div class="row">
                            <div class="col-md-6 form-it">
                                <label>@lang('site.Username')</label>
                                <input type="text" name="name" value="{{ auth()->user()->name }}" placeholder="edwardkennedy">
                            </div>
                            <div class="col-md-6 form-it">
                                <label>@lang('site.Email Address')</label>
                                <input type="text" name="email" value="{{ auth()->user()->email }}" placeholder="edward@kennedy.com">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <input class="submit" type="submit" name="update" value="@lang('site.update')">
                            </div>
                        </div>
                    </form>
                    <form action="{{ url()->full() }}" method="POST" class="password">
                        @csrf
                        <h4>02. @lang('site.Change password')</h4>
                        <div class="row">
                            <div class="col-md-6 form-it">
                                <label>@lang('site.Old Password')</label>
                                <input type="password" name="old" placeholder="**********">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-it">
                                <label>@lang('site.New Password')</label>
                                <input type="password" name="password" placeholder="***************">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-it">
                                <label>@lang('site.Confirm New Password')</label>
                                <input type="password" name="password_confirmation" placeholder="*************** ">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <input class="submit" type="submit" name="pass" value="@lang('site.password')">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- profile section-->

@endsection
