@extends('front.layouts.app')
@section('title', auth()->user()->name)

@section('content')

<div class="hero user-hero">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1>{{ auth()->user()->name }}’s @lang('site.'.request()->what)</h1>
					<ul class="breadcumb">
						<li class="active"><a href="{{ url('/') }}">@lang('site.home')</a></li>
						<li> <span class="ion-ios-arrow-right"></span>@lang('site.profile')</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- profile section-->
<div class="page-single">
    <div class="container">
        <div class="row ipad-width2">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="user-information">
                    <div class="user-img">
                        <a href="#"><img src="{{ auth()->user()->image }}" alt=""><br></a>
                        {{-- <a href="#" class="redbtn">@lang('site.Change avatar')</a> --}}
                    </div>
                    <div class="user-fav">
                        <p>@lang('site.Account Details')</p>
                        <ul>
                            <li><a href="{{ route('profile') }}">@lang('site.profile')</a></li>
                            <li class="{{ request()->what == 'faved' ? 'active' : '' }}"><a href="{{ route('user_movies', 'faved') }}">@lang('site.Favorite movies')</a></li>
                            <li class="{{ request()->what == 'later' ? 'active' : '' }}"><a href="{{ route('user_movies', 'later') }}">@lang('site.Saved movies')</a></li>
                            <li class="{{ request()->what == 'watched' ? 'active' : '' }}"><a href="{{ route('user_movies', 'watched') }}">@lang('site.Watched movies')</a></li>
                            {{-- <li><a href="userrate.html">@lang('site.Rated movies')</a></li> --}}
                        </ul>
                    </div>
                    <div class="user-fav">
                        <p>@lang('site.Others')</p>
                        <ul>
                            <li><a href="{{ route('profile') }}">@lang('site.Change password')</a></li>
                            <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('site.Log out')</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="topbar-filter user">
                    <p>@lang('site.found') <span>{{ $movies->total() }} @lang('site.movies')</span> @lang('site.in_total')</p>
					<label>@lang('site.sort_by'):</label>
					<select onChange="window.location.href=this.value">
						<option value="">@lang('site.sort_by')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'rate' ,'sortDir' => 'asc']) }}">@lang('site.rating_ascending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'rate' ,'sortDir' => 'desc']) }}">@lang('site.rating_descending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'year' ,'sortDir' => 'asc']) }}">@lang('site.date_ascending')</option>
						<option value="{{ request()->fullUrlWithQuery(['sortKey' => 'year' ,'sortDir' => 'desc']) }}">@lang('site.date_descending')</option>
					</select>
					{{-- <a href="movielist.html" class="list"><i class="ion-ios-list-outline "></i></a>
					<a  href="moviegrid.html" class="grid"><i class="ion-grid active"></i></a> --}}
                </div>
                <div class="flex-wrap-movielist grid-fav">
                    @foreach ($movies as $item)
                        <div class="movie-item-style-2 movie-item-style-1 style-3">
                            <img src="{{ $item->poster }}" alt="{{ $item->name }}">
                            <div class="hvr-inner">
                                <a  href="{{ route('movies.single', $item->slug) }}"> @lang('site.read_more') <i class="ion-android-arrow-dropright"></i> </a>
                            </div>
                            <div class="mv-item-infor">
                                <h6><a href="{{ route('movies.single', $item->slug) }}">{{ $item->name }}</a></h6>
                                <p class="rate"><i class="ion-android-star"></i><span>{{ $item->rate }}</span> /10</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="topbar-filter">
                    <label>@lang('site.movies') @lang('site.per_page'):</label>
                    <p>{{ $movies->perPage() }}</p>
					{{-- <select>
						<option value="range">20 Movies</option>
						<option value="saab">10 Movies</option>
					</select> --}}

					<div class="pagination2">
                        <span>@lang('site.page') {{ $movies->currentPage() }} @lang('site.of') {{ $movies->lastPage() }}:</span>
                        {{ $movies->links() }}
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- profile section-->

@endsection
