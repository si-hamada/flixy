@extends('front.layouts.app')
@section('title', $movie->name)

@section('content')

<div class="hero movie_single_hero mv-single-hero" style="background: url('{{ $movie->image }}') no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- <h1> movie listing - list</h1>
				<ul class="breadcumb">
					<li class="active"><a href="#">Home</a></li>
					<li> <span class="ion-ios-arrow-right"></span> movie listing</li>
				</ul> -->
            </div>
        </div>
    </div>
</div>
<div class="page-single movie-single movie_single">
    <div class="container">
        <div class="row ipad-width2">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="movie-img sticky-sb">
                    <img src="{{ $movie->poster }}" alt="{{ $movie->name }}">
                    <div class="movie-btn">
                        <div class="btn-transform transform-vertical red">
                            <div><a href="#" class="item item-1 redbtn"> <i class="ion-play"></i> @lang('site.watch_trailer')</a>
                            </div>
                            <div><a href="{{ $movie->trailer }}"
                                    class="item item-2 redbtn fancybox-media hvr-grow"><i class="ion-play"></i></a>
                            </div>
                        </div>
                        {{-- <div class="btn-transform transform-vertical">
                            <div><a href="#" class="item item-1 yellowbtn"> <i class="ion-card"></i> Buy ticket</a>
                            </div>
                            <div><a href="#" class="item item-2 yellowbtn"><i class="ion-card"></i></a></div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
                <div class="movie-single-ct main-content">
                    <h1 class="bd-hd">{{ $movie->name }} <span>{{ $movie->year }}</span></h1>
                    <div class="social-btn">
                        <a href="javascript:;" class="parent-btn add-list" id="add-faved" data-url="{{ route('movies.faved', $movie->id) }}">
                            <i class="ion-heart {{ isset($movie->user()->pivot->faved) && $movie->user()->pivot->faved ? 'saved' : '' }}"></i>
                            @lang('site.favorite')
                        </a>
                        <a href="javascript:;" class="parent-btn add-list" id="add-later" data-url="{{ route('movies.later', $movie->id) }}">
                            <i class="ion-android-bookmark {{ isset($movie->user()->pivot->later) && $movie->user()->pivot->later ? 'saved' : '' }}"></i>
                            @lang('site.save')
                        </a>
                        <a href="javascript:;" class="parent-btn add-list" id="add-watched" data-url="{{ route('movies.watched', $movie->id) }}">
                            <i class="ion-eye {{ isset($movie->user()->pivot->watched) && $movie->user()->pivot->watched ? 'saved' : '' }}"></i>
                            @lang('site.watched')
                        </a>
                        <div class="hover-bnt">
                            <a href="javascript:;" class="parent-btn"><i class="ion-android-share-alt"></i>@lang('site.share')</a>
                            <div class="hvr-item">
                                <a target="_blank" href="https://www.facebook.com/sharer.php?u={{ url()->full() }}" class="hvr-grow"><i class="ion-social-facebook"></i></a>
                                <a target="_blank" href="https://www.twitter.com/share?url={{ url()->full() }}" class="hvr-grow"><i class="ion-social-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                    <div id="movie-rate" class="movie-rate">
                        <div class="rate">
                            <i class="ion-android-star"></i>
                            <p><span>{{ $movie->rate }}</span> /10<br>
                                <span class="rv">{{ $movie->reviews->count() }} @lang('site.reviews')</span>
                            </p>
                        </div>
                        <div class="rate-star">
                            <p>@lang('site.rate_this_movie'): </p>
                            @for ($i = 1; $i < 11; $i++)
                                <i data-num="{{ $i }}" data-rate="{{ $movie->rate }}" class="rate-stars ion-ios-star{{ $movie->rate < $i ? '-outline' : '' }}"></i>
                            @endfor
                        </div>
                    </div>

                    <!-- start | Player -->
                    <div id="player"></div>
                    <!-- end | Player -->

                    <div class="movie-tabs">
                        <div class="tabs">
                            <ul class="tab-links tabs-mv">
                                <li class="active"><a href="#overview">@lang('site.overview')</a></li>
                                <li><a href="#reviews"> @lang('site.reviews')</a></li>
                                <li><a href="#cast"> @lang('site.cast_crew') </a></li>
                                <li><a href="#media"> @lang('site.media')</a></li>
                                <li><a href="#moviesrelated"> @lang('site.related_movies')</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="overview" class="tab active">
                                    <div class="row">
                                        <div class="col-md-8 col-sm-12 col-xs-12">
                                            <p>{{ $movie->description }}</p>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-12">
                                            <div class="sb-it">
                                                <h6>@lang('site.type'):</h6>
                                                <p>{{ $movie->type }}</p>
                                            </div>
                                            <div class="sb-it">
                                                <h6>@lang('site.tags'):</h6>
                                                <p>
                                                    @foreach ($movie->tags as $tag)
                                                        <a href="{{ route('movies', 'tags[]=' . $tag->id) }}">{{ $tag->name }}{{ $loop->last ? '' : ',' }} </a>
                                                    @endforeach
                                                </p>
                                            </div>
                                            <div class="sb-it">
                                                <h6>@lang('site.release') @lang('site.year'):</h6>
                                                <p>{{ $movie->year }}</p>
                                            </div>
                                            <div class="sb-it">
                                                <h6>@lang('site.duration'):</h6>
                                                <p>{{ $movie->duration }}</p>
                                            </div>
                                            <div class="ads">
                                                <img src="{{ asset('front') }}/images/uploads/ads1.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="reviews" class="tab review">
                                    <div class="row">
                                        <div class="rv-hd">
                                            <div class="div">
                                                <h3>@lang('site.related_movies') @lang('site.to')</h3>
                                                <h2>{{ $movie->name }}</h2>
                                            </div>
                                            <a id="to-movie-rate" href="#movie-rate" class="redbtn">@lang('site.write_review')</a>
                                        </div>
                                        <div class="topbar-filter m-2">
                                            <p>@lang('site.found') <span>{{ $movie->reviews->count() }} @lang('site.reviews')</span> @lang('site.in_total')</p>
                                        </div>
                                        @php $reviews = $movie->reviews()->paginate() @endphp
                                        @forelse ($reviews as $review)
                                            <div class="mv-user-review-item">
                                                <div class="user-infor">
                                                    <img src="{{ $review->user->image }}" alt="">
                                                    <div>
                                                        <h3>{{ $review->title }}</h3>
                                                        <div class="no-star">
                                                            @for ($i = 1; $i < 11; $i++)
                                                                <i class="ion-android-star {{ $review->rate < $i ? 'last' : '' }}"></i>
                                                            @endfor
                                                        </div>
                                                        <p class="time">
                                                            {{ $review->created_at->diffForHumans() }} by <a href="#"> {{ $review->user->name }}</a>
                                                        </p>
                                                    </div>
                                                </div>
                                                <p>{{ $review->description }}</p>
                                            </div>
                                        @empty
                                            <h4>@lang('site.no_data_found')</h4>
                                        @endforelse
                                        <div class="topbar-filter">
                                            <div class="pagination2">
                                                <span>@lang('site.page') {{ $reviews->currentPage() }} @lang('site.of') {{ $reviews->lastPage() }}:</span>
                                                {{ $reviews->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="cast" class="tab">
                                    <div class="row">
                                        <h3>@lang('site.cast_crew') @lang('site.of')</h3>
                                        <h2>{{ $movie->name }}</h2>
                                        <!-- //== -->
                                        <div class="mvcast-item">

                                            @forelse ($movie->crews as $crew)
                                                <div class="cast-it">
                                                    <div class="cast-left">
                                                        <img width="50" height="50" src="{{ $crew->image }}" alt="{{ $crew->name }}">
                                                        <a href="#">{{ $crew->name }}</a>
                                                    </div>
                                                    <p>{{ $crew->pivot->crewAs }}</p>
                                                </div>
                                            @empty
                                                <h4>@lang('site.no_data_found')</h4>
                                            @endforelse
                                        </div>
                                        <!-- //== -->
                                    </div>
                                </div>
                                <div id="media" class="tab">
                                    <div class="row">
                                        <div class="rv-hd">
                                            <div>
                                                <h3>@lang('site.media') @lang('site.of')</h3>
                                                <h2>{{ $movie->name }}</h2>
                                            </div>
                                        </div>
                                        <div class="title-hd-sm">
                                            <h4>@lang('site.media') <span>({{ $movie->gallery->count() }})</span></h4>
                                        </div>
                                        <div class="mvsingle-item">
                                            @forelse ($movie->gallery as $file)
                                                <a class="img-lightbox" data-fancybox-group="gallery" href="{{ $file->getFullUrl() }}"><img style="height: 100px;width: 100px;" src="{{ $file->getFullUrl() }}" alt="{{ $movie->name }}"></a>
                                            @empty
                                                <h4>@lang('site.no_data_found')</h4>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                                <div id="moviesrelated" class="tab">
                                    <div class="row">
                                        <h3>@lang('site.related_movies') @lang('site.to')</h3>
                                        <h2>{{ $movie->name }}</h2>
                                        @forelse ($movie->related as $item)
                                            <div class="movie-item-style-2">
                                                <img src="{{ $item->poster }}" alt="{{ $item->name }}">
                                                <div class="mv-item-infor">
                                                    <h6><a href="{{ route('movies.single', $item->slug) }}">{{ $item->name }} <span>({{ $item->year }})</span></a></h6>
                                                    <p class="rate"><i class="ion-android-star"></i><span>{{ $item->rate }}</span> /10</p>
                                                    <p class="describe">{!! Str::limit($item->description, 200, ' ...') !!}</p>
                                                    <p class="run-time"> @lang('site.duration'): {{ $item->duration }} .
                                                        <span>@lang('site.release'): {{ $item->year }}</span></p>
                                                </div>
                                            </div>
                                        @empty
                                            <h4>@lang('site.no_data_found')</h4>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> {{-- end movie-single-ct --}}
            </div> {{-- end col- --}}
        </div> {{-- end ipad-width2 --}}
    </div> {{-- end container --}}
</div> {{-- end movie-single --}}

<!--review form popup-->
<div class="login-wrapper"  id="review-content">
    <div style="width: 465px;" class="login-content">
        <a href="#" class="close">x</a>
        @auth
            <h3>@lang('site.rate_this_movie')</h3>
            <form id="reg-form" method="post" action="{{ route('movies.review', $movie->id) }}">
                @csrf
                <input type="hidden" name="movie_id" value="{{ $movie->id }}">
                <input type="hidden" name="user_id" value="{{ auth()->id() }}">
                <input type="hidden" name="rate" value="10">
                <div class="rate-star">
                    @for ($i = 1; $i < 11; $i++)
                    <i data-num="{{ $i }}" data-rate="{{ $movie->rate }}" class="rate-stars ion-ios-star{{ $movie->rate < $i ? '-outline' : '' }}"></i>
                    @endfor
                </div>

                <div class="row">
                    <label for="title">
                        @lang('site.title'):
                        <input type="text" name="title" id="title" placeholder="Title" pattern="^[a-zA-Z 0-9-_\.]{8,150}$" required="required"/>
                    </label>
                </div>
                <div class="row">
                    <label for="description">
                        @lang('site.description'):
                        <textarea name="description" id="description" style="height: 150px;" required="required"></textarea>
                    </label>
                </div>
                <div class="row">
                    <button type="submit">@lang('site.send')</button>
                </div>
            </form>
        @else
            <h3>@lang('site.log_to_review')</h3>
        @endauth
    </div>
</div>
<!--end of review form popup-->
@endsection

@push('scripts')
<script>
    var file = "[Auto]";
    @isset($movie->movieFiles->first()->id)
        @if($movie->type == 'Movie')
            file =
                "[Auto]{{ url('storage/enc/MovieFiles/'. $movie->movieFiles->first()->id .'/'. $movie->movieFiles->first()->getFirstMedia('path')->name .'.m3u8') }}," +
                "[360]{{ url('storage/enc/MovieFiles/'. $movie->movieFiles->first()->id .'/'. $movie->movieFiles->first()->getFirstMedia('path')->name .'_50.m3u8') }}," +
                "[480]{{ url('storage/enc/MovieFiles/'. $movie->movieFiles->first()->id .'/'. $movie->movieFiles->first()->getFirstMedia('path')->name .'_100.m3u8') }}," +
                "[720]{{ url('storage/enc/MovieFiles/'. $movie->movieFiles->first()->id .'/'. $movie->movieFiles->first()->getFirstMedia('path')->name .'_150.m3u8') }}";
        @else
            file = [
                @foreach($movie->movieFiles as $theFile)
                {
                    "title":"{{ $theFile->name }}",
                    "file":
                        "[Auto]{{ url('storage/enc/MovieFiles/'. $theFile->id .'/'. $theFile->getFirstMedia('path')->name .'.m3u8') }}," +
                        "[360]{{ url('storage/enc/MovieFiles/'. $theFile->id .'/'. $theFile->getFirstMedia('path')->name .'_50.m3u8') }}," +
                        "[480]{{ url('storage/enc/MovieFiles/'. $theFile->id .'/'. $theFile->getFirstMedia('path')->name .'_100.m3u8') }}," +
                        "[720]{{ url('storage/enc/MovieFiles/'. $theFile->id .'/'. $theFile->getFirstMedia('path')->name .'_150.m3u8') }}"
                    },
                @endforeach
                ];
        @endif
    @endisset

    var player = new Playerjs({
        id:"player",
        file: file,
        poster: "{{ $movie->image }}",
        default_quality: "Auto",
        });
</script>
@endpush
