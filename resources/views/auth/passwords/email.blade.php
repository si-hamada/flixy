<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('backend-files') }}/assets/images/favicon.png">
    <title>Elite Admin Template - The Ultimate Multipurpose admin template</title>

    <!-- page css -->
    <link href="{{ asset('backend-files') }}/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('backend-files') }}/dist/css/style.min.css" rel="stylesheet">

    <style>
        #loginform {display: none}
        #recoverform {display: block}
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-default card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url({{ asset('backend-files') }}/assets/images/background/login-register.jpg);">
            <div class="login-box card">
                <div class="card-body">
                    <form class="form-horizontal form-material" id="loginform" method="post" action="{{ url('/login') }}">
                        @csrf
                        <h3 class="box-title m-b-20">@lang('auth.login.title')</h3>
                        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-danger' : '' }}">
                            <div class="col-xs-12">
                                <input required="" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('auth.email')">
                            </div>
                                @if ($errors->has('email'))
                                    <span class="form-control-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <div class="col-xs-12">
                                <input required="" type="password" class="form-control" placeholder="@lang('auth.password')" name="password">
                            </div>
                            @if ($errors->has('password'))
                                <span class="form-control-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-control custom-checkbox">
                                    <input name="remember" type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">@lang('auth.remember_me')</label>
                                    <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> @lang('auth.login.forgot_password')</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">@lang('auth.sign_in')</button>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                                <div class="social">
                                    <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                    <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                @lang('auth.login.not_registerd') <a href="{{ url('/register') }}" class="text-info m-l-5"><b>@lang('auth.login.register_membership')</b></a>
                            </div>
                        </div> --}}
                    </form>
                    <form class="form-horizontal" id="recoverform" method="post" action="{{ url('/password/email') }}">
                        @csrf
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <h3>@lang('auth.forgot_password.title')</h3>
                                <p class="text-muted">@lang('auth.forgot_password.forgot_pwd_desc') </p>
                            </div>
                        </div>
                        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-danger' : '' }}">
                            <div class="col-xs-12">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('auth.email')" required="">
                                @if ($errors->has('email'))
                                    <span class="form-control-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">@lang('auth.forgot_password.send_pwd_reset')</button>
                            </div>
                            <div class="col-xs-12 m-t-20 text-center">
                                <a href="javascript:void(0)" id="to-login" class="text-dark"><i class="fa fa-lock m-r-5"></i> @lang('auth.forgot_password.back_to_login')</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/popper/popper.min.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!--Custom JavaScript -->
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        // ==============================================================
        // Login and Recover Password
        // ==============================================================
        $('#to-recover').on("click", function() {
            $("#loginform").slideUp();
            $("#recoverform").fadeIn();
        });
        $('#to-login').on("click", function() {
            $("#recoverform").slideUp();
            $("#loginform").fadeIn();
        });
    </script>

</body>

</html>
