<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('backend-files') }}/assets/images/favicon.png">
    <title>@lang('auth.register') - {{ config('app.name') }}</title>

    <!-- page css -->
    <link href="{{ asset('backend-files') }}/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('backend-files') }}/dist/css/style.min.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">{{ config('app.name') }} admin</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background-image:url({{ asset('front') }}/images/uploads/error-bg.jpg);background-position: bottom;">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="loginform" method="post" action="{{ url('/register') }}">
                    @csrf

                    <a href="{{ url('/') }}" class="text-center db"><img src="{{ asset('backend-files') }}/assets/images/logo-icon.png" alt="Home" /><br/></a>
                    <h3 class="box-title m-t-40 m-b-0">@lang('auth.register')</h3>
                    <small>@lang('auth.registration.create_account_enjoy')</small>

                    <div class="form-group m-t-20 has-feedback {{ $errors->has('name') ? ' has-danger' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" name="name" value="{{ old('name') }}" placeholder="@lang('auth.full_name')">
                        </div>
                        @if ($errors->has('name'))
                            <span class="form-control-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('email') ? ' has-danger' : '' }}">
                        <div class="col-xs-12">
                            <input required="" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="@lang('auth.email')">
                        </div>
                        @if ($errors->has('email'))
                            <span class="form-control-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password') ? ' has-danger' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" name="password" placeholder="@lang('auth.password')">
                        </div>
                        @if ($errors->has('password'))
                            <span class="form-control-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" name="password_confirmation" placeholder="@lang('auth.confirm_password')">
                        </div>
                        @if ($errors->has('password_confirmation'))
                            <span class="form-control-feedback">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group row has-feedback {{ $errors->has('terms') ? ' has-danger' : '' }}">
                        <div class="col-md-12">
                            <div class="custom-control custom-checkbox">
                                <input name="terms" type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1"> @lang('auth.registration.i_agree') <a href="javascript:void(0)">@lang('auth.registration.terms')</a></label>
                            </div>
                            @if ($errors->has('terms'))
                                <span class="form-control-feedback">
                                    <strong>{{ $errors->first('terms') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">@lang('auth.register')</button>
                        </div>
                    </div>
                    <p>@lang('auth.or_via_social')</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social">
                                <a href="{{ url('/login/facebook') }}" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                <a href="{{ url('/login/google') }}" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>@lang('auth.registration.have_membership') <a href="{{ url('/login') }}" class="text-info m-l-5"><b>@lang('auth.sign_in')</b></a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('backend-files') }}/assets/node_modules/popper/popper.min.js"></script>
    <script src="{{ asset('backend-files') }}/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $(".preloader").fadeOut();
        });
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
</body>

</html>
